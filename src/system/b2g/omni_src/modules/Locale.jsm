this.EXPORTED_SYMBOLS=["Locale"];const{classes:Cc,interfaces:Ci,utils:Cu}=Components;Cu.import("resource://gre/modules/Services.jsm");Cu.import("resource://gre/modules/Preferences.jsm");const PREF_MATCH_OS_LOCALE="intl.locale.matchOS";const PREF_SELECTED_LOCALE="general.useragent.locale";this.Locale={getLocale(){if(Preferences.get(PREF_MATCH_OS_LOCALE,false))
return Services.locale.getLocaleComponentForUserAgent();try{let locale=Preferences.get(PREF_SELECTED_LOCALE,null,Ci.nsIPrefLocalizedString);if(locale)
return locale;}
catch(e){}
return Preferences.get(PREF_SELECTED_LOCALE,"en-US");},findClosestLocale(aLocales){let appLocale=this.getLocale(); var bestmatch=null; var bestmatchcount=0; var bestpartcount=0;var matchLocales=[appLocale.toLowerCase()];if(matchLocales[0].substring(0,3)!="en-")
matchLocales.push("en-us");for(let locale of matchLocales){var lparts=locale.split("-");for(let localized of aLocales){for(let found of localized.locales){found=found.toLowerCase(); if(locale==found)
return localized;var fparts=found.split("-");if(bestmatch&&fparts.length<bestmatchcount)
continue; var maxmatchcount=Math.min(fparts.length,lparts.length);var matchcount=0;while(matchcount<maxmatchcount&&fparts[matchcount]==lparts[matchcount])
matchcount++;if(matchcount>bestmatchcount||(matchcount==bestmatchcount&&fparts.length<bestpartcount)){bestmatch=localized;bestmatchcount=matchcount;bestpartcount=fparts.length;}}} 
if(bestmatch)
return bestmatch;}
return null;},};