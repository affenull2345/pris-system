'use strict';
(function(exports) {

    var firstSeven = ['\u2640', '\u2642', '\u2620', '\u2622', '\u2623', '\u26a0', '\u26a1'], emojiPack = [], pageSize = 12;
    for(let cp=0x1f600;cp<0x1f650;cp++) {
      let diff = cp - 0x1f600;
      let index = Math.floor(diff / pageSize) | 0, localIndex = diff % pageSize;
      if(!emojiPack[index]) emojiPack[index] = []
      emojiPack[index][localIndex] = String.fromCodePoint(cp)
    }

    var Symbols = function(app) {
        this.app = app;
        this._isActive = false;
        this.isDefaultSoftkeyBar = false;
        this.pendingDeactivating = false;
        this.pageIndex = 0;
        this.focusIndex = 0;
        Object.defineProperty(this, 'isPortrait', {
            get: function() {
                return screen.orientation.type.startsWith('portrait');
            }
        });
        Object.defineProperty(this, 'PAGES', {
            get: function() {
                if (this.isPortrait) {
                    if (this.app.inputLanguage && this.app.inputLanguage.indexOf('spanish') > -1) {
                        return [
                            ['.', ',', '@', '¡', '¿', '-', ':', '&', '\'', '(', ')', '#'],
                            ['+', '=', '/', '!', '?', '*', ';', '$', '"', '<', '>', '%'],
                            ['^', '_', '\\', '~', '`', '|', '[', ']', '©', '{', '}', '®'],
                            ['£', '¥', '€'].concat(firstSeven)
                        ].concat(emojiPack);
                    } else {
                        return [
                            ['.', ',', '@', '!', '?', '-', ':', '&', '\'', '(', ')', '#'],
                            ['+', '=', '/', '¡', '¿', '*', ';', '$', '"', '<', '>', '%'],
                            ['^', '_', '\\', '~', '`', '|', '[', ']', '©', '{', '}', '®'],
                            ['£', '¥', '€'].concat(firstSeven)
                        ].concat(emojiPack);
                    }
                } else {
                    if (this.app.inputLanguage && this.app.inputLanguage.indexOf('spanish') > -1) {
                        return [
                            ['.', ',', '@', '¡', '¿', '-', ':', '&', '\'', '#'],
                            ['+', '=', '/', '!', '?', '*', ';', '(', ')', '"'],
                            ['<', '>', '%', '$', '^', '_', '\\', '~', '`', '|'],
                            ['[', ']', '©', '{', '}', '®', '£', '¥', '€', '']
                        ];
                    } else {
                        return [
                            ['.', ',', '@', '!', '?', '-', ':', '&', '\'', '#'],
                            ['+', '=', '/', '¡', '¿', '*', ';', '(', ')', '"'],
                            ['<', '>', '%', '$', '^', '_', '\\', '~', '`', '|'],
                            ['[', ']', '©', '{', '}', '®', '£', '¥', '€', '']
                        ];
                    }
                }
            }
        });
    };
    Symbols.prototype.NUMBER_KEYS = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '0', '#'];
    Symbols.prototype.ARROW_KEYS = ['ArrowUp', 'ArrowRight', 'ArrowDown', 'ArrowLeft'];
    Symbols.prototype.FUNCTION_KEYS = ['SoftLeft', 'SoftRight', 'Enter', 'Backspace'];
    Symbols.prototype._isNumberKey = function(key) {
        return this.NUMBER_KEYS.indexOf(key) > -1;
    };
    Symbols.prototype._isArrowKey = function(key) {
        return this.ARROW_KEYS.indexOf(key) > -1;
    };
    Symbols.prototype._isFunctionKey = function(key) {
        return this.FUNCTION_KEYS.indexOf(key) > -1;
    };
    Symbols.prototype.start = function() {
        this._startComponents();
    };
    Symbols.prototype._grid = function() {
        var portrait = this.isPortrait;
        var container = document.getElementById('symbols-container');
        this.PAGES[this.pageIndex].forEach((symbol, index) => {
            var area = document.createElement('div');
            area.classList.add('symbol-area');
            area.setAttribute('tabIndex', '-1');
            area.innerHTML = '<div class="symbol-frame"><span class="symbol"></span></div>';
            if (portrait) {
                area.dataset.number = this.NUMBER_KEYS[index];
            }
            container.appendChild(area);
        });
    };
    Symbols.prototype._startComponents = function() {
        this.header = document.getElementById('symbols-header');
        if (!this.isPortrait) {
            this.header.dataset.l10nId = 'symbols-lnd';
        }
        this._grid();
        this.container = document.getElementById('symbols');
        this.items = document.getElementsByClassName('symbol-area');
        this.softKeys = document.getElementById('soft-keys');
        this.setActive(false);
    };
    Symbols.prototype._monitor = function() {
        this.inputContext = navigator.mozInputMethod.inputcontext;
        if (this.inputContext) {
            this.isDefaultSoftkeyBar = this.inputContext.defaultSoftkeyBar || !this.inputContext.isFromApp;
            this.inputContext.addEventListener('keydown', this);
            this.inputContext.addEventListener('keypress', this);
            this.inputContext.addEventListener('keyup', this);
        } else {
            if (this._isActive) {
                this.setActive(false);
            }
        }
    };
    Symbols.prototype._render = function() {
        this.PAGES[this.pageIndex].forEach((symbol, index) => {
            this.items[index].dataset.symbol = symbol;
            this.items[index].getElementsByClassName('symbol')[0].textContent = symbol;
        });
        this.header.dataset.l10nArgs = JSON.stringify({
            n: this.pageIndex
        });
    };
    Symbols.prototype._next = function() {
        this.pageIndex = (this.pageIndex + 1) % this.PAGES.length;
        this._render();
    };
    Symbols.prototype._previous = function() {
        var newIndex = this.pageIndex - 1;
        if (newIndex < 0) {
            newIndex = newIndex + this.PAGES.length;
        }
        this.pageIndex = newIndex % this.PAGES.length;
        this._render();
    };
    Symbols.prototype._updateSoftKeys = function() {
        if (this._isActive) {
            this.softKeys.children[0].dataset.l10nId = 'previous';
            this.softKeys.children[1].removeAttribute('data-icon');
            this.softKeys.children[1].dataset.l10nId = 'select';
            this.softKeys.children[2].dataset.l10nId = 'next';
        } else {
            this.softKeys.children[0].textContent = '';
            this.softKeys.children[0].dataset.l10nId = '';
            this.softKeys.children[1].textContent = '';
            this.softKeys.children[1].dataset.l10nId = 'enter';
            this.softKeys.children[1].dataset.icon = 'enter';
            this.softKeys.children[2].dataset.l10nId = 'done';
        }
        this.softKeys.style.opacity = this._isActive || this.isDefaultSoftkeyBar ? 1 : 0;
        this.softKeys.classList.toggle('browser', this.isDefaultSoftkeyBar);
    };
    Symbols.prototype._sendSymbolByKey = function(key) {
        var candidate, symbol;
        if (key === 'Backspace') {
            symbol = '';
        } else if (key === 'Enter') {
            candidate = this.items[this.focusIndex];
            symbol = candidate.dataset.symbol;
        } else {
            candidate = this.container.querySelector('[data-number="' + key + '"]');
            symbol = candidate.dataset.symbol;
        }
        this.inputContext.setComposition(symbol);
        this.inputContext.endComposition(symbol);
        this.pendingDeactivating = true;
    };
    Symbols.prototype._focus = function(key) {
        var isRtl = document.dir === 'rtl';
        var nextIndex;
        var numberCol = this.container.clientWidth / this.items[0].clientWidth;
        if (key) {
            switch (key) {
                case 'ArrowRight':
                    nextIndex = this.focusIndex + 1;
                    break;
                case 'ArrowLeft':
                    nextIndex = this.focusIndex - 1;
                    break;
                case 'ArrowDown':
                    nextIndex = this.focusIndex + numberCol;
                    break;
                case 'ArrowUp':
                    nextIndex = this.focusIndex - numberCol;
                    break;
                default:
                    break;
            }
            nextIndex = (nextIndex + this.items.length) % this.items.length;
        } else {
            nextIndex = isRtl ? numberCol - 1 : 0;
        }
        this.items[this.focusIndex].classList.remove('current');
        if (this.items[nextIndex].dataset.symbol !== '') {
            this.items[nextIndex].classList.add('current');
        } else {
            while (this.items[nextIndex].dataset.symbol === '') {
                if (key === 'ArrowRight') {
                    nextIndex++;
                } else {
                    nextIndex--;
                }
                nextIndex = (nextIndex + this.items.length) % this.items.length;
            }
            this.items[nextIndex].classList.add('current');
        }
        this.focusIndex = nextIndex;
    };
    Symbols.prototype._handleKeydown = function(event) {
        var key = event.detail.key;
        if (this.app.inputLanguage === 'thai') {
            return;
        }
        if (!this._isActive && key === '*') {
            if (this.app.t9.isSuggesting) {
                this.app.t9._reset();
            }
            if (this.app.t9.isComposing) {
                this.app.t9._endComposing();
            }
            this.setActive(true);
            return;
        }
        if (this._isActive && key !== 'Power') {
            event.preventDefault();
            event.stopImmediatePropagation();
            if (this._isNumberKey(key)) {
                if (this.isPortrait) {
                    this._sendSymbolByKey(key);
                }
            } else if (this._isFunctionKey(key)) {
                if (key === 'SoftRight') {
                    this._next();
                } else if (key === 'SoftLeft') {
                    this._previous();
                } else if (key === 'Enter' || key === 'Backspace') {
                    this._sendSymbolByKey(key);
                }
            } else if (this._isArrowKey(key)) {
                this._focus(key);
            }
        }
    };
    Symbols.prototype._handleKeypress = function(event) {
        if (this._isActive && event.detail.key !== 'Power') {
            event.preventDefault();
            event.stopImmediatePropagation();
        }
    };
    Symbols.prototype._handleKeyup = function(event) {
        if (this._isActive && event.detail.key !== 'Power') {
            event.preventDefault();
            event.stopImmediatePropagation();
            if (this.pendingDeactivating) {
                this.setActive(false);
            }
        }
    };
    Symbols.prototype.setActive = function(option) {
        this._isActive = option;
        this.pendingDeactivating = false;
        this.container.classList.toggle('show', option);
        this._updateSoftKeys();
        if (option === false) {
            this.pageIndex = 0;
            this._render();
        } else {
            this._focus();
        }
    };
    Symbols.prototype.isActive = function() {
        return this._isActive;
    };
    Symbols.prototype.handleEvent = function(event) {
        switch (event.type) {
            case 'keydown':
                this._handleKeydown(event);
                break;
            case 'keypress':
                this._handleKeypress(event);
                break;
            case 'keyup':
                this._handleKeyup(event);
                break;
            default:
                break;
        }
    };
    exports.Symbols = Symbols;
})(window);
