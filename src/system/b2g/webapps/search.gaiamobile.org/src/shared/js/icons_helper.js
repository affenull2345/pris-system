'use strict';(function IconsHelper(exports){function getIcon(uri,iconSize,placeObj){var icon;if(placeObj&&placeObj.icons){icon=getBestIcon(placeObj.icons,iconSize);}
if(!icon){var a=document.createElement('a');a.href=uri;icon=a.origin+'/favicon.ico';}
return new Promise(resolve=>{resolve(icon);});}
function getBestIcon(icons,iconSize){if(!icons){return null;}
var options=getSizes(icons);var sizes=Object.keys(options).sort(function(a,b){return a-b;});if(sizes.length===0){var iconStrings=Object.keys(icons);return iconStrings.length>0?iconStrings[0]:null;}
var preferredSize=getPreferredSize(sizes,iconSize);var icon=options[preferredSize];if(icon.rel==='apple-touch-icon'){var iconsUrl='https://developer.mozilla.org/en-US/'+'Apps/Build/Icon_implementation_for_apps#General_icons_for_web_apps';console.warn('Warning: The apple-touch icons are being used '+'as a fallback only. They will be deprecated in '+'the future. See '+iconsUrl);}
return icon.uri;}
function getSizes(icons){var sizes={};var uris=Object.keys(icons);uris.forEach(function(uri){var uriSizes=icons[uri].sizes.join(' ').split(' ');uriSizes.forEach(function(size){var sizeValue=guessSize(size);if(!sizeValue){return;}
sizes[sizeValue]={uri:uri,rel:icons[uri].rel};});});return sizes;}
function getPreferredSize(sizes,iconSize){var targeted=iconSize?parseInt(iconSize):0;if(targeted===0){targeted=window.devicePixelRatio>1?142:84;}
var selected=-1;var length=sizes.length;for(var i=0;i<length&&selected<targeted;i++){selected=sizes[i];}
return selected;}
function guessSize(size){var xIndex=size.indexOf('x');if(!xIndex){return null;}
return size.substr(0,xIndex);}
exports.IconsHelper={getIcon:getIcon,getBestIcon:getBestIcon,getSizes:getSizes};})(window);