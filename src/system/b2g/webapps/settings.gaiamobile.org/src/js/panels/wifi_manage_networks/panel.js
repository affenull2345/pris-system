
define('panels/wifi_manage_networks/panel',['require','shared/settings_listener','modules/settings_panel'],function(require) {
  
  var SettingsListener = require('shared/settings_listener');
  var SettingsPanel = require('modules/settings_panel');

  return function ctor_wifi_manage_networks_panel() {
    var _macAddress;
    var elements = document.querySelectorAll('#wifi-manageNetworks li');

    function _initSoftkey() {
      var params = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };

      SettingsSoftkey.init(params);
      SettingsSoftkey.show();
    }

    function _updateSoftkey() {
      var focusedElement = document.querySelector('#wifi-manageNetworks .focus');
      if (focusedElement.classList.contains('none-select')) {
        SettingsSoftkey.hide();
      } else {
        SettingsSoftkey.show();
      }
    }

    function _initFocusEventListener() {
      var i = elements.length - 1;
      for (i; i >= 0; i--) {
        elements[i].addEventListener('focus', _updateSoftkey);
      }
    }

    function _removeFocusEventListener() {
      var i = elements.length - 1;
      for (i; i >= 0; i--) {
        elements[i].removeEventListener('focus', _updateSoftkey);
      }
    }

    return SettingsPanel({
      onInit: function(panel) {
        _macAddress = panel.querySelector('[data-name="deviceinfo.mac"]');
        // we would update this value all the time
        SettingsListener.observe('deviceinfo.mac', '', macAddress => {
          _macAddress.textContent = macAddress;
        });
      },

      onBeforeShow: function() {
        _initSoftkey();
        _initFocusEventListener();
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
        _removeFocusEventListener();
      }
    });
  };
});
