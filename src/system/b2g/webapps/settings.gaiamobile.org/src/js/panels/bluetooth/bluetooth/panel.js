/**
 * The Bluetooth panel
 *
 */
define(['require','shared/toaster','modules/bluetooth/bluetooth_context','modules/settings_panel','modules/settings_service'],function(require) {
  

  var Toaster = require('shared/toaster');
  var BtContext = require('modules/bluetooth/bluetooth_context');
  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');

  const VISIBLE_TIMEOUT_TIME = 120000;  // Visibility will timeout after 2 mins.

  var _debug = false;
  var debug = function() {};
  if (_debug) {
    debug = function btp_debug(msg) {
      console.log('--> [Bluetooth][Panel]: ' + msg);
    };
  }

  return function ctor_bluetooth() {
    var elements;
    var listElements = document.querySelectorAll('#bluetooth li');

    function _updateSoftkey() {
      var focusedElement = document.querySelector('#bluetooth .focus');
      if (focusedElement.classList.contains('none-select')) {
        SettingsSoftkey.hide();
      } else {
        SettingsSoftkey.show();
      }
    }

    function _initFocusEventListener() {
      var i = listElements.length - 1;
      for (i; i >= 0; i--) {
        listElements[i].addEventListener('focus', _updateSoftkey);
      }
    }

    function _removeFocusEventListener() {
      var i = listElements.length - 1;
      for (i; i >= 0; i--) {
        listElements[i].removeEventListener('focus', _updateSoftkey);
      }
    }

    return SettingsPanel({
      onInit: function(panel) {
        debug('onInit():');

        // Init bounding instances for observe/un-observe property.
        this._boundUpdateEnableCheckbox = this._updateEnableCheckbox.bind(this);
        this._boundUpdateBluetoothList = this._updateBluetoothList.bind(this);
        this._boundUpdatePhoneName = this._updatePhoneName.bind(this);
        this._boundUpdateVisibleCheckbox = this._updateVisibleCheckbox.bind(this);

        elements = {
          panel: panel,
          enableMsg: panel.querySelector('#bluetooth-enable-msg'),
          enableCheckbox: panel.querySelector('.bluetooth-status input'),
          bluetoothSelect: panel.querySelectorAll('input[name="bluetooth-enabled"]'),

          visibleItem: panel.querySelector('#device-visible'),
          visibleCheckBox: panel.querySelector('.device-visible input'),
          visibleSelect: panel.querySelector('#device-visible select'),

          myPhoneNameItem: panel.querySelector('#myPhone-name'),
          phoneName: panel.querySelector('#bluetooth-device-name'),
          renameItem: panel.querySelector('#bluetooth-rename'),

          pairedDevicesItem: panel.querySelector('#bluetooth-paired-device'),

          nearbyDeviceItem: panel.querySelector('#bluetooth-devices-nearby'),

          items: panel.querySelectorAll('li')
        };

        for (var i = 0; i < elements.bluetoothSelect.length; i++) {
          elements.bluetoothSelect[i].addEventListener('change',
          this._onEnableCheckboxClick.bind(this, i));
        }

        elements.visibleSelect.addEventListener('change',
          this._onVisibleCheckBoxClick.bind(this));
      },

      onBeforeShow: function() {
        debug('onBeforeShow():');

        this._initSoftkey();

        // enable/disable
        BtContext.observe('state', this._boundUpdateEnableCheckbox);
        this._updateEnableCheckbox(BtContext.state);

        BtContext.observe('discoverable', this._boundUpdateVisibleCheckbox);
        this._updateVisibleCheckbox(BtContext.discoverable);

        BtContext.observe('state', this._boundUpdateBluetoothList);
        this._updateBluetoothList(BtContext.state);

        BtContext.observe('name', this._boundUpdatePhoneName);
        this._updatePhoneName(BtContext.name);

        window.addEventListener('keydown', this._keyDownHandler);
        _initFocusEventListener();
      },

      onShow: function() {
        debug('onShow():');
      },

      onBeforeHide: function() {
        debug('onBeforeHide():');
        SettingsSoftkey.hide();
        BtContext.unobserve('state', this._boundUpdateEnableCheckbox);
        BtContext.unobserve('discoverable', this._boundUpdateVisibleCheckbox);
        BtContext.unobserve('name', this._boundUpdatePhoneName);
        BtContext.unobserve('state', this._boundUpdateBluetoothList);
        window.removeEventListener('keydown', this._keyDownHandler);
        _removeFocusEventListener();
      },

      onHide: function() {
        debug('onHide():');
      },

      _onEnableCheckboxClick: function(i) {
        var checkboxEnabled = (elements.bluetoothSelect[i].value === 'true');
        debug('_onEnableCheckboxClick(): checkbox = ' + checkboxEnabled);
        var status = checkboxEnabled ? 'on' : 'off';
        BtContext.setEnabled(checkboxEnabled).then(() => {
          let toast = {
            messageL10nId: 'bluetooth-current-status-' + status,
            latency: 2000,
            useTransition: true
          };
          Toaster.showToast(toast);
          debug('_onEnableCheckboxClick(): setEnabled ' +
                checkboxEnabled + ' successfully');
        }, (reason) => {
          debug('_onEnableCheckboxClick(): setEnabled ' +
                checkboxEnabled + ' failed, reason = ' + reason);
        });
      },

      _onVisibleCheckBoxClick: function() {
        var checkbox = (elements.visibleSelect.value === 'true');
        debug('_onVisibleCheckBoxClick(): checked = ' + checkbox);
        var status = checkbox ? 'on' : 'off';
        BtContext.setDiscoverable(checkbox).then(() => {
          let toast = {
            messageL10nId: 'bluetooth-visible-status-' + status,
            latency: 2000,
            useTransition: true
          };
          Toaster.showToast(toast);
          debug('_onVisibleCheckBoxClick(): setDiscoverable ' +
                checkbox + ' successfully');
        }, (reason) => {
          debug('_onVisibleCheckBoxClick(): setDiscoverable ' +
                checkbox + ' failed, reason = ' + reason);
        });
      },

      _updateBluetoothList: function(state) {
        var booleanFlag = state === 'enabled' ? false : true;

        elements.visibleItem.hidden = booleanFlag;
        elements.myPhoneNameItem.hidden = booleanFlag;
        elements.renameItem.hidden = booleanFlag;
        elements.nearbyDeviceItem.hidden = booleanFlag;
        elements.pairedDevicesItem.hidden = booleanFlag;
        elements.enableMsg.hidden = !booleanFlag;

        if (state === 'enabled' || state === 'disabled') {
          if (Settings.currentPanel === '#bluetooth') {
            var evt = new CustomEvent('panelready', {
              detail:{
                current:Settings.currentPanel
              }
            });
            window.dispatchEvent(evt);
          }
        }

      },

      _updateEnableCheckbox: function(state) {
        state === 'enabled' ? elements.bluetoothSelect[0].checked = true :
                              elements.bluetoothSelect[1].checked = true;
      },

      _updateVisibleCheckbox: function(discoverable) {
        elements.visibleSelect.options[0].selected = discoverable;
        elements.visibleSelect.options[1].selected = !discoverable;
      },

      _updatePhoneName: function(name) {
        debug('_updatePhoneName(): ' +
              'callback from observe "name" = ' + name);
        elements.phoneName.textContent = name;
      },

      _initSoftkey: function() {
      var params = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };
      SettingsSoftkey.init(params);
      SettingsSoftkey.show();
    },

    _keyDownHandler: function(e) {
      switch (e.key) {
        case 'Accept':
        case 'Enter':
          var checkboxValue = document.querySelector('.focus input');
            if (checkboxValue && checkboxValue.value === 'false' && checkboxValue.checked) {
              SettingsService.navigate('root');
            }
          break;
      }
    }

    });
  };
});
