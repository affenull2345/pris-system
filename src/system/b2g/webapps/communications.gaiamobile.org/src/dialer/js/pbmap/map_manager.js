'use strict';
/* global PbmapIAC, MapMessage */
(function(exports) {

  var bluetoothManager = navigator.mozBluetooth;
  var adapter = null;
  var deviceName;

  var message;
  const masId = 0;
  const SMS = 'msg';
  const MMS = null;
  const INBOX = 'inbox';
  const OUTBOX = 'outbox';
  const SENT = 'sent';

  function isBtV2() {
    if (typeof(bluetoothManager.onattributechanged) !== 'undefined') {
      return true;
    } else {
      return false;
    }
  }

  function mapInit() {
    message = new MapMessage();
    if (!adapter) {
      return;
    }
    adapter.addEventListener('mapmessageslistingreq',
      mapMessagesListing.bind(this));
    adapter.addEventListener('mapgetmessagereq',
      mapGetMessage.bind(this));
    adapter.addEventListener('mapsetmessagestatusreq',
      mapSetMessageStatus.bind(this));
    adapter.addEventListener('mapsendmessagereq',
      mapSendMessage.bind(this));
    adapter.addEventListener('mapnotifyreq', mapNotify.bind(null, adapter));
    mapNotifyInit(adapter);
  }

  function init() {
    if (isBtV2()) {
      watchMozBluetoothAttributechanged();
      initDefaultAdapter();
    } else {
      fetchDefaultAdapter();

      // 'adapteradded' can only be triggered by enabling Bluetooth.
      // update adapter when the Bluetooth enabled again.
      bluetoothManager.onadapteradded = function() {
        fetchDefaultAdapter();
      };
    }
  }

  function fetchDefaultAdapter() {
    var req = bluetoothManager.getDefaultAdapter();
    req.onsuccess = function bt_getAdapterSuccess() {
      if (adapter) {
        adapter.onpbapconnectionreq = null;
        adapter.onobexpasswordreq = null;
        adapter.onpullphonebookreq = null;
        adapter.onpullvcardentryreq = null;
        adapter.onpullvcardlistingreq = null;
      }

      adapter = req.result;
      bindMapRq();
    };
    req.onerror = function bt_getAdapterFailed() {
      console.error('MAP', 'ERROR adapter');
    };
  }

  function mapNotify(adapter, evt) {
    var notifyObj = {
      handle: evt.detail.message.id
    };
    switch (evt.detail.type) {
      case 'received':
        notifyObj.type = 'newMessage';
        notifyObj.folder = 'inbox';
        break;
      case 'deliverysuccess':
        notifyObj.type = 'deliverySuccess';
        notifyObj.folder = 'outbox';
        break;
      case 'deliveryerror':
        notifyObj.type = 'deliveryFailure';
        notifyObj.folder = 'outbox';
        break;
      case 'sent':
        notifyObj.type = 'sendingSuccess';
        notifyObj.folder = 'outbox';
        break;
      case 'failed':
        notifyObj.type = 'sendingFailure';
        notifyObj.folder = 'outbox';
        break;
    }
    var eventObj = window.reformatEventObj(notifyObj);
    var blob = new Blob([eventObj], {type: 'text/xml'});
    adapter.sendMessageEvent(masId, blob);
  }

  function mapNotifyInit(adapter) {
    message.messageEventInit(adapter);
  }

  function bindMapRq() {
    if (adapter) {
      adapter.addEventListener('mapconnectionreq', getConfirm.bind(this));
    }
  }

  function getConfirm(evt) {
    function callSystemConfirm(text) {
      navigator.mozL10n.formatValue('confirmTitle').then(title => {
        var option = {
          type: PbmapIAC.USER_CONFIRMATION,
          header: title,
          message: text,
          profile: 'MAP'
        };
        PbmapIAC.callSystemDialog(option).then(result => {
          if (result) {
            evt.handle.accept();
            obexInit();
            mapInit();
          } else {
            evt.handle.reject();
          }
        }).catch(err => {
          console.error(err);
          evt.handle.reject();
        });
      });
    }

    var getNamePromise = new Promise((resolve, reject) => {
      var req = adapter.getPairedDevices();
      if (isBtV2()) {
        req.forEach((item, index, array) => {
          if (item.address === evt.address) {
            if (item.name) {
              resolve(item.name);
            } else {
              // listen for name update.
              item.onattributechanged = function (evt) {
                for (var i in evt.attrs) {
                  if (evt.attrs[i] === 'name') {
                    resolve(item.name);
                    // clean event handler
                    item.onattributechanged = null;
                  }
                }
              };
              //set a timeout in case the device has no name
              setTimeout(()=> {
                // clean event handler
                item.onattributechanged = null;

                // return address instead
                resolve(evt.address);
              }, 2000);
            }
          } else {
            if (index === (array.length - 1)) {
              resolve(evt.address);
            }
          }
        });
      } else {
        req.onsuccess = function () {
          this.result.forEach((item, index, array) => {
            if (item.address === evt.address) {
              resolve(item.name);
            } else {
              if (index === (array.length - 1)) {
                resolve(evt.address);
              }
            }
          });
        };
        req.onerror = function () {
          reject('failed to get the adapter');
        };
      }

    });

    getNamePromise.then(name => {
      deviceName = name;
      return deviceName;
    }).then(nameText => {
      callSystemConfirm(nameText);
    });
  }

  function enterObexPinCode(evt) {
    function callPasswordDialog(text) {
      var responseValues = ['responseTitle', 'initialValue'];
      Promise.all(responseValues.map(key => navigator.mozL10n.formatValue(key)))
      .then(values => {

        var option = {
          type: PbmapIAC.OBEX_PASSWORD,
          header: values[0],
          message: text,
          initialValue: values[1]
        };

        PbmapIAC.callSystemDialog(option).then(result => {
          if (result.value) {
            evt.handle.setPassword(result.password);
          } else {
            evt.handle.reject();
          }
        });
      });
    }

    navigator.mozL10n
      .formatValue('responseMsg', {deviceId: deviceName}).then(responseText => {
      callPasswordDialog(responseText);
    });
  }

  function obexInit () {
    adapter.addEventListener('obexpasswordreq', enterObexPinCode);
  }

  function mapMessagesListing(evt) {
    var filter = getMsgType(evt.name);
    filter.readStatus = evt.filterReadStatus;
    message.getAllMessages(filter).then((messages) => {
      return window.reformatXMLMsg(messages);
    }).then((data) => {
      var blob = new Blob([data.xml], {type: 'text/xml'});
      evt.handle.replyToMessagesListing(masId, blob, data.unreadFlag, getMSTime(), data.size);
    });
  }

  function getMsgType(name) {
    var filter = {};
    if (name.indexOf(SMS) !== -1) {
      filter.type = 'sms';
    } else if (name.indexOf(MMS) !== -1) {
      filter.type = 'mms';
    } else {
      filter.type = 'email';
    }

    if (name.indexOf(INBOX) !== -1) {
      filter.dir = 'received';
    } else if (name.indexOf(OUTBOX) !== -1) {
      filter.dir = 'sending';
    } else if (name.indexOf(SENT) !== -1) {
      filter.dir = 'sent';
    }

    return filter;
  }

  function getMSTime() {
    Date.prototype.hhmmss = function() {
      var hh = this.getHours().toString();
      var mm = this.getMinutes().toString();
      var ss = this.getSeconds().toString();
      return (hh[1]? hh : '0' + hh[0]) + (mm[1]? mm : '0' + mm[0]) + (ss[1]? ss : '0' + ss[0]);
    };
    Date.prototype.YYYYMMDD = function() {
      var YYYY = this.getFullYear().toString();
      var MM = (this.getMonth() + 1).toString();
      var DD  = this.getDate().toString();
      return YYYY + (MM[1]? MM : '0' + MM[0]) + (DD[1]? DD : '0' + DD[0]);
    };
    var date = new Date();
    return date.YYYYMMDD() + 'T' + date.hhmmss();
  }


  function mapGetMessage(evt) {
    message.getMessage(evt).then(message => {
      return window.reformatBMessage(message);
    }).then(data => {
      var blob = new Blob([data], {type: 'text/xml'});
      evt.handle.replyToGetMessage(masId, blob);
    });
  }

  function mapSetMessageStatus(evt) {
    if (evt.statusIndicator === 'readstatus') {
      message.setReadStatus(evt).then(result => {
        evt.handle.replyToSetMessageStatus(masId, result);
      });
    } else if (evt.statusIndicator === 'deletedstatus') {
      message.setDeletedStatus(evt).then(result => {
        evt.handle.replyToSetMessageStatus(masId, result);
      });
    }
  }

  function mapSendMessage(evt) {
    message.sendMessage(evt).then(id => {
      evt.handle.replyToSendMessage(masId, id, true);
    }).catch(id => {
      evt.handle.replyToSendMessage(masId, id, false);
    });
  }

  function watchMozBluetoothAttributechanged() {
    bluetoothManager.addEventListener('attributechanged', (evt) => {
      for (var i in evt.attrs) {
        switch (evt.attrs[i]) {
          case 'defaultAdapter':
            initDefaultAdapter();
            break;
          default:
            break;
        }
      }
    });
  }

  function initDefaultAdapter() {
    if (adapter) {
      adapter.onpbapconnectionreq = null;
      adapter.onobexpasswordreq = null;
      adapter.onpullphonebookreq = null;
      adapter.onpullvcardentryreq = null;
      adapter.onpullvcardlistingreq = null;
    }

    adapter = bluetoothManager.defaultAdapter;
    bindMapRq();
  }

  init();

})(window);
