'use strict';var ConferenceGroupUI=(function(){var groupCalls=document.getElementById('group-call-details'),groupCallsList=document.getElementById('group-call-details-list'),bdiGroupCallsCountElt=document.createElement('bdi'),initialized=false;function _init(callback){if(initialized){callback();}
LazyLoader.load([groupCalls],function(){groupCallsList=document.getElementById('group-call-details-list');if(!initialized){bdiGroupCallsCountElt=document.createElement('bdi');}
initialized=true;callback();});}
function hideGroupDetails(evt){if(evt){evt.preventDefault();}
if(!navigator.mozTelephony.conferenceGroup.calls.length){_removeAllCalls();}
groupCalls.classList.remove('display');}
function _removeAllCalls(){var callNodes=groupCalls.querySelectorAll('.handled-call');for(var i=0;i<callNodes.length;i++){removeCall(callNodes[i]);}}
function createItem(info){var listItem=document.createElement('li');listItem.classList.add('conference_group_list');listItem.classList.add('p-pri');var infoItem=document.createElement('div');infoItem.textContent=info;listItem.appendChild(infoItem);return listItem;}
function addCall(info){var item=createItem(info);groupCallsList.appendChild(item);return item;}
function addImsCall(number,name){var item=createImsItem(number,name);groupCallsList.appendChild(item);return item;}
function createImsItem(number,name){var listItem=document.createElement('li');listItem.classList.add('conference_group_list');listItem.classList.add('p-pri');listItem.dataset.telenumber=number;var infoItem=document.createElement('div');if(name===''){infoItem.textContent=number;}else{infoItem.textContent=name;}
listItem.appendChild(infoItem);return listItem;}
function removeCall(node){groupCallsList.removeChild(node);}
function showGroupDetails(){NavigationManager.reset('.conference_group_list');groupCalls.classList.add('display');}
function removeDetails(){groupCallsList.innerHTML='';}
function endCall(){if(isImsconferenceGroup()){endImsCall();}else{hideGroupDetails();var listItem=groupCalls.querySelector('.focus');if(listItem.endCall){listItem.endCall();}}}
function isImsconferenceGroup(){var firstListItem=groupCallsList.childNodes[0];if(groupCallsList.childElementCount==0||firstListItem.endCall==undefined){return true;}else{return false;}}
function endImsCall(){hideGroupDetails();var listItem=groupCalls.querySelector('.focus');var endCallNumber=listItem.dataset.telenumber;var calls=navigator.mozTelephony.conferenceGroup.calls;for(var i=0;i<calls.length;i++){if(endCallNumber==calls[i].id.number){calls[i].hangUp();return;}}}
function sperateCall(){hideGroupDetails();var listItem=groupCalls.querySelector('.focus');if(listItem.speratecall){listItem.speratecall();}else{var endCallNumber=listItem.dataset.telenumber;var calls=navigator.mozTelephony.conferenceGroup.calls;calls.find((call)=>{if(endCallNumber===call.id.number){window.navigator.mozTelephony.conferenceGroup.remove(call);return true;}});}}
function isGroupDetailsShown(){return groupCalls.classList.contains('display');}
function markCallsAsEnded(){_init(function(){var callElems=groupCallsList.getElementsByTagName('SECTION');for(var i=0;i<callElems.length;i++){callElems[i].dataset.groupHangup='groupHangup';}});}
function setGroupDetailsHeader(text){bdiGroupCallsCountElt.textContent=text;}
return{addCall:addCall,addImsCall:addImsCall,removeCall:removeCall,showGroupDetails:showGroupDetails,hideGroupDetails:hideGroupDetails,isGroupDetailsShown:isGroupDetailsShown,markCallsAsEnded:markCallsAsEnded,endCall:endCall,sperateCall:sperateCall,removeDetails:removeDetails,isImsconferenceGroup:isImsconferenceGroup,setGroupDetailsHeader:setGroupDetailsHeader};})();