
define(['require','exports','module','tmpl!./msg/recipients_cc_peep_bubble.html','cards','l10n!','toaster','contacts','./base','template!./message_reader_recipients_cc.html'],function(require, exports, module) {
  var recipientsCcPeepBubbleNode =
      require('tmpl!./msg/recipients_cc_peep_bubble.html');
  var cards = require('cards');
  var mozL10n = require('l10n!');
  var toaster = require('toaster');
  var Contacts = require('contacts');
  var instance;
  var currentFocusPeep = -1;
  var addressNode = new Array();

  return [
    require('./base')(require('template!./message_reader_recipients_cc.html')),
    {
      onArgs: function(args) {
        this.composer = args.composer;
        this.type = args.type;
        instance = this;
        if (this.type === 'recipients') {
          this.recipientsCcHeader =
              mozL10n.get('msg-reader-recipients-header') +
              ' (' + this.composer.header.to.length + ')';
          this.contactsList = this.composer.header.to;
        } else if (this.type === 'cc') {
          this.recipientsCcHeader =
              mozL10n.get('msg-reader-cc-header') +
              ' (' + this.composer.header.cc.length + ')';
          this.contactsList = this.composer.header.cc;
        }
      },

      onBack: function() {
        cards.removeCardAndSuccessors(this, 'animate');
      },

      sendNewMail: function() {
        cards.eatEventsUntilNextCard();
        cards.pushCard('compose', 'animate', {
          composerData: {
            message: instance.composer.header,
            onComposer: function(composer) {
              composer.to = [{
                address: currentFocusPeep.address,
                name: currentFocusPeep.name
              }];
            }
          }
        });
      },

      viewContact: function() {
        console.log('viewContact, currentFocusPeep is ' + currentFocusPeep);
        var a = new MozActivity({
          name: 'open',
          data: {
            type: 'webcontacts/contact',
            params: {
              'id': currentFocusPeep.contactId
            }
          }
        });
        a.onsuccess = function() {
          console.log('view contact info success');
          NavigationMap.setFocus('restore');
        };
        a.onerror = function() {
          console.log('view contact info failed');
          NavigationMap.setFocus('restore');
        };
      },

      saveNewContact: function() {
        console.log('saveNewContact, currentFocusPeep is ' + currentFocusPeep);
        var params = {
          'email': currentFocusPeep.address
        };

        if (currentFocusPeep.name) {
          params.givenName = currentFocusPeep.name;
        }

        var a = new MozActivity({
          name: 'new',
          data: {
            type: 'webcontacts/contact',
            params: params
          }
        });
        a.onsuccess = function() {
          console.log('save new contact success');
          NavigationMap.setFocus('restore');
        };
        a.onerror = function() {
          console.log('save new contact failed');
          NavigationMap.setFocus('restore');
        };
      },

      saveExistContact: function() {
        var a = new MozActivity({
          name: 'update',
          data: {
            type: 'webcontacts/contact',
            params: {
              'email': currentFocusPeep.address
            }
          }
        });
        a.onsuccess = function() {
          console.log('save to existing contact success');
          NavigationMap.setFocus('restore');
        };
        a.onerror = function() {
          console.log('save to existing contact failed');
          setTimeout( () => {
            NavigationMap.setFocus('restore');
          }, 400);
        };
      },

      updatePeepInfo: function(event) {
        console.log('updatePeepInfo, event.detail.peep.address is ' +
                     event.detail.peep.address);
        if (event.detail.peep.address === currentFocusPeep.address) {
          document.querySelector('[data-email="' + currentFocusPeep.address +
                                 '"]').textContent = event.detail.peep.name ||
                                                     event.detail.peep.address;
          var contactImageNode = document.querySelector('[data-email="' +
                                 currentFocusPeep.address + '"]')
                                 .previousElementSibling;
          if (contactImageNode) {
            var contactImageLabel =
                contactImageNode.querySelector('.contact-image-label');
            if (event.detail.photo) {
              console.log('updatePeepInfo, event.detail.photo has value');
              contactImageLabel.src = URL.createObjectURL(event.detail.photo);
            } else {
              contactImageLabel.src =
                  '../../style/images/icons/contacts_icon.png';
            }
          }
          instance.setSoftKey();
        }
      },

      die: function() {
        window.removeEventListener('keydown', this.handleKeydown);
        window.removeEventListener('contact-updated', this.updatePeepInfo);
      },

      onCardVisible: function(navDirection) {
        console.log(this.localName + '.onCardVisible, navDirection=' +
                    navDirection);
        window.addEventListener('keydown', this.handleKeydown);
        window.addEventListener('contact-updated', this.updatePeepInfo);

        this._refresh();
        this._refreshImage();
        this.setSoftKey();
        if (navDirection === 'forward') {
          NavigationMap.setFocus('first');
        } else if(navDirection === 'back') {
          NavigationMap.setFocus('restore');
        }
      },

      onHidden: function() {
        console.log(this.localName + ' onHidden');
        window.removeEventListener('keydown', this.handleKeydown);
        window.removeEventListener('contact-updated', this.updatePeepInfo);
      },

      navSetup: function() {
        var CARD_NAME = this.localName;
        var QUERY_CHILD = '.msg-reader-peep-focusable';
        var CONTROL_ID = CARD_NAME + ' ' + QUERY_CHILD;

        NavigationMap.navSetup(CARD_NAME, QUERY_CHILD);
        NavigationMap.setCurrentControl(CONTROL_ID);
      },

      handleKeydown: function(evt) {
        switch (evt.key) {
          case 'Backspace':
            evt.preventDefault();
            if (option.menuVisible == false) {
              instance.onBack();
            }
            break;
        }
      },

      _refresh: function() {
        if (this.contactsList && this.contactsList.length) {
          this.recipientsCcContainer.innerHTML = '';
          for (var i = 0; i < this.contactsList.length; i++) {
            var peepBubbleTemplate = recipientsCcPeepBubbleNode.cloneNode(true);
            var contactName = peepBubbleTemplate
                .querySelector('.message-reader-peep-bubble-name');
            contactName.textContent =
                this.contactsList[i].name || this.contactsList[i].address;
            contactName.setAttribute('data-email',
                this.contactsList[i].address);
            this.recipientsCcContainer.appendChild(peepBubbleTemplate);
            addressNode.push({
              address: this.contactsList[i].address,
              node: peepBubbleTemplate
            });
          }
        }
        this.recipientsCcLabel.innerHTML = this.recipientsCcHeader;
        this.navSetup();
      },

      _refreshImage: function() {
        addressNode.forEach(function(item) {
          Contacts.findContactByString(item.address, item.node,
              instance.updateImage);
        });
      },

      updateImage: function(contacts, inputValue, insertNode) {
        if (contacts.length === 0 || contacts[0].photo === null) {
          return;
        }
        var blob = contacts[0].photo[0];
        if (blob) {
          var contactImage = insertNode.querySelector(
              '.message-reader-peep-bubble-picture' +
              ' .contact-image-label');
          contactImage.src = URL.createObjectURL(blob);
        }
      },

      setSoftKey: function() {
        var params = [];
        params.push({
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          method: function() {
            instance.onBack();
          }
        });
        params.push({
          name: 'Send New Mail',
          l10nId: 'send-new-mail',
          priority: 5,
          method: function() {
            instance.sendNewMail();
          }
        });
        if (currentFocusPeep.isContact) {
          params.push({
            name: 'View Contact',
            l10nId: 'view-contact',
            priority: 5,
            method: function() {
              instance.viewContact();
            }
          });
        } else {
          params.push({
            name: 'Save New Contact',
            l10nId: 'save-new-contact',
            priority: 5,
            method: function() {
              instance.saveNewContact();
            }
          });
          Contacts.getCount().then((count) => {
            if (count) {
              params.push({
                name: 'Add to Existing Contact',
                l10nId: 'add-to-exsiting-contact',
                priority: 5,
                method: function() {
                  instance.saveExistContact();
                }
              });
            }
          });
        }
        NavigationMap.setSoftKeyBar(params);
      },

      onFocusChanged: function(queryChild, index, item) {
        console.log(this.localName + '.onFocusChanged, queryChild=' +
                    queryChild + ', index=' + index);
        currentFocusPeep = this.contactsList[index];
        this.setSoftKey();
      }
    }
  ];
});
