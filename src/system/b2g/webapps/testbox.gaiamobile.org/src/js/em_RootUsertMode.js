

/**
 * Created by gaoguang on 9/31/16.
 */

'use strict';

// ------------------------------------------------------------------------
var isfirst = true;
var engmodeEx =  navigator.engmodeExtension;
var _self = {};

function $(id){
  return document.getElementById(id);
};

var Prop = [
  {key:'ro.secure', rootValue:0, userValue:1},
  {key:'ro.allow.mock.location', rootValue:1, userValue:0},
  {key:'ro.debuggable', rootValue:1, userValue:0},
  {key:'persist.service.adb.enable', rootValue:1, userValue:0}//,
  //{key:'service.adb.root', rootValue:1, userValue:0}
];



var RootUserMode = {
  init: function _init(){
    _self = this;

    if(true == isfirst)
    {
      _self.addListeners();
      isfirst = false;

    };

    if (_self.checkProperty('root')) {
      $('_mode_head').innerHTML = "Root Mode";
    } else if(_self.checkProperty('user')){
      $('_mode_head').innerHTML = "User Mode";
    } 
  },
  addListeners: function _addListeners(){
    $('Root2User').addEventListener('click', function() {
        _self.root2User();
    });

    $('User2Root').addEventListener('click', function() {
        _self.user2Root();
    });

  },

  root2User: function _root2User() {
    dump("[RootUserMode] root2User");
  //      _self.dumpProperty();
    _self.setProperty('user');

    window.setTimeout(function() {
      if (_self.checkProperty('user')) {
        $('_mode_head').innerHTML = "User Mode";
        _self.rebootAdbdProcess();
      }
        }, 500);

  },

  user2Root: function _user2Root() {
    dump("[RootUserMode] user2Root");
  //      _self.dumpProperty();
    _self.setProperty('root');

    window.setTimeout(function() {
          if (_self.checkProperty('root')) {
            $('_mode_head').innerHTML = "Root Mode";
            _self.rebootAdbdProcess();
          }
        }, 500);
   
  },

  rebootAdbdProcess: function _rebootAdbdProcess() {
    if(engmodeEx) {
      var initRequest = engmodeEx.execCmdLE(['killadbd'], 1);
    }
  },

  setProperty: function _setProperty(type) {
    if (!engmodeEx) {
      dump("[RootUserMode] engmode is null");
      return;
    }
    switch (type) {
      case 'root':
        for (var item in Prop) {
          engmodeEx.setPropertyValue(Prop[item].key, Prop[item].rootValue);
          dump("[RootUserMode] setProperty key:" + Prop[item].key + "  value:" + Prop[item].rootValue);
        }
        break;
      case 'user':
        for (var item in Prop) {
          engmodeEx.setPropertyValue(Prop[item].key, Prop[item].userValue);
          dump("[RootUserMode] setProperty key:" + Prop[item].key + "  value:" + Prop[item].userValue);
        }
        break;
    }
  },

  dumpProperty: function _dumpProperty(){
    var msg = "";
    for (var item in Prop) {
         var value = engmodeEx.getPropertyValue(Prop[item].key);
          msg += "key:" + Prop[item].key + "  value:" + value + "     <br>   ";
      }
    dump("[RootUserMode]" + msg);
      return msg;
  },

  checkProperty: function _checkProperty(type) {
    var ret = true;
    for (var item in Prop) {
         var value = engmodeEx.getPropertyValue(Prop[item].key);
        dump("[RootUserMode] type:" + type + "  value:" + value + "  key:" + Prop[item].key);
         if (type == 'root') {
            if (value != Prop[item].rootValue){
              ret = false;
            } 
         } else if (type == 'user'){
            if (value != Prop[item].userValue){
                ret = false;
              } 
         }
         
      }
      return ret;
  }
};


RootUserMode.init();
