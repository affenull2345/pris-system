/* -*- Mode: js; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- /
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */

/*global Compose, Recipients, Utils, AttachmentMenu, Template, Settings,
         SMIL, ErrorDialog, MessageManager, LinkHelper,
         ActivityPicker, ThreadListUI, OptionMenu, Threads, Contacts,
         Attachment, WaitingScreen, MozActivity, LinkActionHandler,
         ActivityHandler, TimeHeaders, ContactRenderer, Draft, Drafts,
         Thread, Navigation, Promise, LazyLoader,
         SharedComponents,
         Errors,
         EventDispatcher,
         SelectionHandler,
         TaskRunner
*/
/*exported ThreadUI */

(function(exports) {
'use strict';

var attachmentMap = new WeakMap();

var skSend = {
  l10nId: 'send',
  priority: 1,
  method: function() {
    ThreadUI.startSendMessage();
  }
};

var skSendSIM1 = {
  l10nId: 'send',
  icon: 'sim-1',
  iconMixedText: true,
  priority: 1,
  method: function () {
    ThreadUI.startSendMessage();
  }
};

var skSendSIM2 = {
  l10nId: 'send',
  icon: 'sim-2',
  iconMixedText: true,
  priority: 1,
  method: function () {
    ThreadUI.startSendMessage();
  }
};

var skLinkSelect = {
  l10nId: 'select',
  priority: 2,
  method: function () {
    ThreadUI.findLinkFocus();
  }
};

var skAttachmentOpen = {
  l10nId: 'open',
  priority: 2,
  method: function () {
    ThreadUI.doLinkAction('attachment');
  }
};

var skDialOpen = {
  l10nId: 'call',
  priority: 2,
  method: function () {
    ThreadUI.doLinkAction('dial');
  }
};

var skEmailOpen = {
  l10nId: 'email',
  priority: 2,
  method: function () {
    ThreadUI.doLinkAction('email');
  }
};

var skUrlOpen = {
  l10nId: 'open',
  priority: 2,
  method: function () {
    ThreadUI.doLinkAction('url');
  }
};

var skOption = {
  l10nId: 'options',
  priority: 3,
  method: function () {
    var focused = document.querySelector('.focus');
    var linkType = ThreadUI.listenLinkElement(focused);
    ThreadUI.openLinkFocus(linkType.replace('-link', ''));
  }
};

var skEnter = {
  l10nId: 'enter',
  priority: 2
};

var skOpen = {
  l10nId: 'open',
  priority: 2
};

var skSelect = {
  l10nId: 'select',
  priority: 2,
  method: function() {
    secondCall = false;
  }
};

var skDownload = {
  l10nId: 'download-attachment',
  priority: 2
};

var skAddContact = {
  l10nId: 'add-contact',
  priority: 3,
  method: function() {
    ThreadUI.requestContact();
  }
};

var skAddSuggestion = {
  l10nId: 'add-suggestion',
  priority: 2,
  method: function() {
    ThreadUI.selectRecipientSuggestion();
    Recipients.View.suggest = true;
  }
};

var skAddSubject = {
  l10nId: 'add-subject',
  priority: 5,
  method: function() {
    ThreadUI.notFocus = true;
    setTimeout(() => {
      ThreadUI.subjectManagement();
    }, 500);
  }
};

var skRemoveSubject = {
  l10nId: 'remove-subject',
  priority: 5,
  method: function() {
    ThreadUI.notFocus = true;
    setTimeout(() => {
      ThreadUI.subjectManagement();
    }, 500);
  }
};

var skAddAttachment = {
  l10nId: 'add-attachment',
  priority: 5,
  method: function() {
    ThreadUI.addEmpty = true;
    setTimeout(() => {
      Compose.onAttachClick();
    }, 80);
  }
};

var skSelectMessages = {
  l10nId: 'select-messages',
  priority: 5,
  method: function() {
    ThreadUI.selectMessages();
  }
};

var skCall = {
  l10nId: 'call',
  priority: 5,
  method: function() {
    ThreadUI.notFocus = true;
    ThreadUI.isOptionsCall = true; // Bug655-gang-chen@t2mobile.com-option menu misplaced
    ActivityPicker.dial(Threads.active.participants[0], false);
    var index = ThreadUI.getItemIndex(
      NavigationMap.getCurrentControl().elements, 'messages-input');
    NavigationMap.setFocus(index);
  }
};

var skDeleteThread = {
  l10nId: 'delete-thread',
  priority: 5,
  method: function() {
    Utils.speedPressPrevent(function() {
      ThreadUI.notFocus = true;
      ThreadUI.deleteCurrentThread('threadMode');
    });
  }
};

var skSaveAsDraft = {
  l10nId: 'sk-save-as-draft',
  priority: 5,
  method: function() {
    ThreadUI.saveDraft();
    setTimeout(function() {
      Navigation.toPanel('thread-list');
    }, 50);
  }
};

var skCancel = {
  l10nId: 'cancel',
  priority: 5,
  method: function() {
    ThreadUI.back();
  }
};

var skForward = {
  l10nId: 'forward',
  priority: 5,
  method: function() {
    var targetItem = ThreadUI._storeFocused;
    var messageId = ThreadUI._getFocusedMessageId(targetItem);
    if (Compose.isEmpty()) {
      ThreadUI._forwardMessage(messageId);
    } else {
      Utils.confirmAlert('confirmation-title', 'draft-save-content',
                         'cancel', null, 'save-as-draft',
                         function() {
                           ThreadUI.saveDraft();
                           ThreadUI._forwardMessage(messageId);
                         }, 'discard-message', function() {
                           ThreadUI.assimilateRecipients('false');
                           ThreadUI.discardDraft();
                           ThreadUI._forwardMessage(messageId);
                         });
    }
  }
};

var skViewMessageReport = {
  l10nId: 'view-message-report',
  priority: 5,
  method: function() {
    var targetItem = ThreadUI._storeFocused;
    var messageId = ThreadUI._getFocusedMessageId(targetItem);
    Navigation.toPanel('report-view', {
      id: messageId,
      threadId: Threads.currentId
    });
    ThreadUI.updateSKs(threadUiCancelReport);

  }
};

var skDelete = {
  l10nId: 'delete',
  priority: 5,
  method: function() {
    Utils.speedPressPrevent(function() {
      if (1 === ThreadUI.allInputs.length) {
        ThreadUI.deleteCurrentThread('messageMode');
      } else {
        ThreadUI.deleteSingleMessage();
      }
    });
  }
};

var skResendThisMessage = {
  l10nId: 'resend-message',
  priority: 5,
  method: function() {
    var targetItem = ThreadUI._storeFocused;
    var messageId = ThreadUI._getFocusedMessageId(targetItem);
    ThreadUI.resendMessage(messageId);
  }
};

var skCancelViewReport = {
  l10nId: 'cancel-view-report',
  priority: 1,
  method: function() {
    Navigation.toPanel('thread', {id: Threads.currentId});
    NavigationMap.disableNav = false;
  }
};


// select messages's softkey
var skSelectAll = {
  l10nId: 'select-all',
  priority: 1,
  method: function() {
    ThreadUI.clickCheckUncheckAllButton();
  }
};

var skDeselectAll = {
  l10nId: 'deselect-all',
  priority: 1,
  method: function() {
    ThreadUI.clickCheckUncheckAllButton();
  }
};

var skDeSelect = {
  l10nId: 'deselect',
  priority: 2
};

var skSelectDelete = {
  l10nId: 'delete',
  priority: 3,
  method: function() {
    if (ThreadUI.isInEditMode()) {
      var selected = ThreadUI.selectionHandler.selectedCount;
      if (selected === ThreadUI.allInputs.length) {
        ThreadUI.deleteCurrentThread('editMode');
      } else {
        ThreadUI.delete();
      }
    }
  }
};

var skRemoveAttachment = {
  l10nId: 'remove-attachment',
  priority: 5,
  method: function() {
    ThreadUI.notFocus = true;
    Compose.removeAttachment();

    // a quick fix, should update softkeys together in other place later
    // now we update options when got option menu hide event, not realy on
    // a random timeout
    window.addEventListener('menuEvent', function updateOptions() {
      window.removeEventListener('menuEvent', updateOptions);
      Compose.focus();
      ThreadUI.dynamicSK();
    });
  }
};

var skReplaceAttachment = {
  l10nId: 'replace-attachment',
  priority: 5,
  method: function() {
    Compose.replaceAttachment();
  }
};

var skLastMessage = {
  l10nId: 'last-message',
  priority: 2
};

var skCancelToThread = {
  l10nId: 'cancel',
  priority: 1,
  method: function() {
    ThreadUI.cancelToThread();
  }
};

var skWait = {
  l10nId: 'wait',
  priority: 1
};

var threadUiComposerOptions = {
  header: {l10nId: 'options'},
  items: [skEnter, skAddSubject, skAddAttachment]
};

var threadUiComposerOptions_noMMS = {
  header: {l10nId: 'options'},
  items: [skEnter]
};

var threadUiComposerOptionsWithAttach = {
  header: { l10nId:'options' },
  items: [skOpen, skRemoveAttachment, skReplaceAttachment,
          skAddAttachment]
};

var threadUiComposerOptionsWithSend = {
  header: { l10nId:'options' },
  items: [skEnter, skAddSubject, skAddAttachment]
};

var threadUiComposerOptionsWithSend_noMMS = {
  header: { l10nId:'options' },
  items: [skEnter]
};

var threadUiComposerOptionsWithSend_Attach = {
  header: { l10nId:'options' },
  items: [skOpen, skRemoveAttachment,
          skReplaceAttachment, skAddAttachment]
};

var threadUiComposerOptionsWithSubRemove = {
  header: { l10nId:'options' },
  items: [skEnter, skRemoveSubject, skAddAttachment]
}

var threadUiComposerOptionsWithSubRemove_subfocused = {
  header: { l10nId:'options' },
  items: [skRemoveSubject, skAddAttachment]
}

var threadUiComposerOptionsWithSubRemove_Attach = {
  header: { l10nId:'options' },
  items: [skOpen, skRemoveAttachment, skReplaceAttachment,
          skRemoveSubject, skAddAttachment]
}

var threadUiComposerOptionsWithSubRemove_Send = {
  header: { l10nId:'options' },
  items: [skEnter, skRemoveSubject, skAddAttachment]
}

var threadUiComposerOptionsWithSubRemove_Send_Subfocused = {
  header: { l10nId:'options' },
  items: [skRemoveSubject, skAddAttachment]
}

var threadUiComposerOptionsWithSubRemove_Send_Attach = {
  header: { l10nId:'options' },
  items: [skOpen, skRemoveAttachment, skReplaceAttachment,
          skRemoveSubject, skAddAttachment]
}

var threadUiNormalOptions = {
  header: { l10nId:'options' },
  items: [skEnter, skAddSubject, skAddAttachment,
          skSelectMessages, skCall, skDeleteThread]
};

var threadUiNormalOptions_noMMS = {
  header: { l10nId:'options' },
  items: [skEnter, skSelectMessages, skCall, skDeleteThread]
};

var threadUiNormalOptionsWithAttach = {
  header: { l10nId:'options' },
  items: [skOpen, skRemoveAttachment, skReplaceAttachment,
          skAddSubject, skAddAttachment, skSelectMessages,
          skCall, skDeleteThread]
};

var threadUiNormalOptionsWithSend = {
  header: { l10nId:'options' },
  items: [skEnter, skAddSubject, skAddAttachment,
          skSelectMessages, skCall, skDeleteThread]
};

var threadUiNormalOptionsWithSend_noMMS = {
  header: { l10nId:'options' },
  items: [skEnter, skSelectMessages, skCall, skDeleteThread]
};

var threadUiNormalOptionsWithSend_Attach = {
  header: { l10nId:'options' },
  items: [skOpen, skRemoveAttachment,
          skReplaceAttachment, skAddAttachment]
};

var threadUiNormalOptionsWithSubRemove = {
  header: { l10nId:'options' },
  items: [skEnter, skRemoveSubject, skAddAttachment,
          skSelectMessages, skCall, skDeleteThread]
};

var threadUiNormalOptionsWithSubRemove_Subfocused = {
  header: { l10nId:'options' },
  items: [skRemoveSubject, skAddAttachment,
          skSelectMessages, skCall, skDeleteThread]
};

var threadUiNormalOptionsWithSubRemove_Attach = {
  header: { l10nId:'options' },
  items: [skOpen, skRemoveAttachment, skReplaceAttachment,
          skRemoveSubject, skAddAttachment, skSelectMessages,
          skCall, skDeleteThread]
};

var threadUiNormalOptionsWithSubRemove_Send = {
  header: { l10nId:'options' },
  items: [skEnter, skRemoveSubject, skAddAttachment,
          skSelectMessages, skCall, skDeleteThread]
};

var threadUiNormalOptionsWithSubRemove_Send_Subfocused = {
  header: { l10nId:'options' },
  items: [skRemoveSubject, skAddAttachment,
          skSelectMessages, skCall, skDeleteThread]
};

var threadUiNormalOptionsWithSubRemove_Send_Attach = {
  header: { l10nId:'options' },
  items: [skOpen, skRemoveAttachment, skReplaceAttachment,
          skRemoveSubject, skAddAttachment, skSelectMessages,
          skCall, skDeleteThread]
};

var threadUiAddContact = {
  header: {l10nId: 'options'},
  items: [skAddContact]
};

var threadUiAddContact_send = {
  header: {l10nId: 'options'},
  items: [skAddContact]
};

var threadUIAddContactWithInput = {
  header: {l10nId: 'options'},
  items: [skSelect, skAddContact]
};

var threadUIComposeRecipient = {
  header: {l10nId: 'options'},
  items: [skSelect, skAddContact]
};

var threadUiAddSuggestion = {
  header: {l10nId: 'options'},
  items: [skAddSuggestion]
};

var threadUiCancelReport={
  header: {l10nId: 'options'},
  items:[skCancelViewReport]
};

var selectAllOptions = {
  header: {l10nId: 'options'},
  items: [skSelectAll, skSelect]
};

var selectAllWithDeleteOptions = {
  header: {l10nId: 'options'},
  items: [skSelectAll, skSelect, skSelectDelete]
};

var selectAllWithDeslectFocus = {
  header: {l10nId: 'options'},
  items: [skSelectAll, skDeSelect, skSelectDelete]
};

var selectDeselectAllwithDeleteOptions = {
  header: {l10nId: 'options'},
  items: [skDeselectAll, skDeSelect, skSelectDelete]
};

var selectAllOptionsWait = {
  header: {l10nId: 'options'},
  items: [skWait, skSelect]
};

var selectAllWithDeleteOptionsWait = {
  header: {l10nId: 'options'},
  items: [skWait, skSelect, skSelectDelete]
};

var selectAllWithDeslectFocusWait = {
  header: {l10nId: 'options'},
  items: [skWait, skDeSelect, skSelectDelete]
};

var promptContactOption = {
  header: {l10nId: 'options'},
  items: [skSelect]
};

var lastMessageOption = {
  header: {l10nId: 'options'},
  items: [skLastMessage]
};

function thui_mmsAttachmentClick(target) {
  var attachment = attachmentMap.get(target);
  if (!attachment) {
    return false;
  }

  attachment.view({
    allowSave: true,
    forEditing: true
  });

  return true;
}

// reduce the Composer.getContent() into slide format used by SMIL.generate some
// day in the future, we should make the SMIL and Compose use the same format
function thui_generateSmilSlides(slides, content) {
  var length = slides.length;
  if (typeof content === 'string') {
    if (!length || slides[length - 1].text) {
      slides.push({
        text: content
      });
    } else {
      slides[length - 1].text = content;
    }
  } else {
    slides.push({
      blob: content.blob,
      name: content.name
    });
  }
  return slides;
}

var ThreadUI = {
  CHUNK_SIZE: 5,
  // duration of the notification that message type was converted
  CONVERTED_MESSAGE_DURATION: 2000,
  IMAGE_RESIZE_DURATION: 3000,
  BANNER_DURATION: 2000,

  // Toast duration when you write a long text and need more than one SMS
  // to send it
  ANOTHER_SMS_TOAST_DURATION: 3000,

  // when sending an sms to several recipients in activity, we'll exit the
  // activity after this delay after moving to the thread list.
  LEAVE_ACTIVITY_DELAY: 3000,

  draft: null,
  recipients: null,
  // Set to |true| when in edit mode
  inEditMode: false,
  shouldChangePanelNextEvent: false,
  showErrorInFailedEvent: '',
  // Set switch flag because the compose -> thread render problem
  switchFlag: false,
  previousSegment: 0,
  scrollIndex: 0,
  // store the item when change focus or open option
  _storeFocused: null,
  currentMode: '',
  currentThreadId: null,
  optionMenuShown: false,
  contactPromptOptionMenuShown: false,
  confirmDialogShown: false,
  currentSoftKey: null,
  optionMenu: null,
  focusableElement: null,
  backspaceTimer: null,
  EndKeySave: false,
  notFocus: false,
  addEmpty: false,
  isDownScroll: false,
  isOptionsCall: false, // Bug655-gang-chen@t2mobile.com-option menu misplaced
  threadIdBackUp: null,

  timeouts: {
    update: null,
    subjectLengthNotice: null
  },

  FOCUS_INVALID: 0,
  FOCUS_ON_RECIPIENTS: 1,
  FOCUS_ON_SUBJECT: 2,
  FOCUS_ON_MESSAGE_INPUT: 3,
  FOCUS_ON_INPUT_IFRAME: 4,
  FOCUS_ON_MESSAGE_THREAD: 5,

  RECIPIENTS_INPUT_FIELD_MAX_ONE: 50,

  init: function thui_init() {
    var threadMessages = document.getElementById('thread-messages');
    if (threadMessages.innerHTML.length === 0) {
      var template = Template('thread-messages-view-tmpl');
      threadMessages.innerHTML = template.toString();
    }
    this.selector = {
      recipient: document.querySelector('#messages-recipients-list'),
      subject: document.querySelector('#subject-composer-input'),
      textField: document.querySelector('#messages-input')
    };
    this.messageInput = document.getElementById('messages-input');
    this.messageInput.dataset.placeholder = navigator.mozL10n.get(
        Utils.camelCase(this.messageInput.id) + '_placeholder'
    );

    this.messagesComposeForm = document.getElementById('messages-compose-form');

    var templateIds = [
      'message',
      'message-sim-information',
      'message-status',
      'not-downloaded',
      'recipient',
      'date-group',
      'header',
      'group-header'
    ];

    AttachmentMenu.init('attachment-options-menu');

    // Fields with 'messages' label
    [
      'container', 'to-field', 'recipients-list', 'compose-form', 'header',
      'edit-header', 'check-uncheck-all-button', 'contact-pick-button',
      'send-button', 'delete-button', 'call-number-button', 'options-button',
      'new-message-notice', 'edit-mode', 'edit-form', 'header-text',
      'max-length-notice', 'convert-notice', 'resize-notice',
      'subject-max-length-notice', 'sms-counter-notice',
      'recipient-suggestions'
    ].forEach(function(id) {
      this[Utils.camelCase(id)] = document.getElementById('messages-' + id);
    }, this);

    this.mainWrapper = document.getElementById('main-wrapper');
    this.threadMessages = document.getElementById('thread-messages');

    window.addEventListener('resize', this.resizeHandler.bind(this));

    // binding so that we can remove this listener later
    this.onVisibilityChange = this.onVisibilityChange.bind(this);
    document.addEventListener('visibilitychange',
                              this.onVisibilityChange);

    this.messageInput.addEventListener(
      'focus', this.onMessageInputFocusChange.bind(this)
    );

    this.messageInput.addEventListener(
      'blur', this.onMessageInputFocusChange.bind(this)
    );

    this.toField.addEventListener(
      'input', this.toFieldInput.bind(this), true
    );

    this.toField.addEventListener(
      'focus', this.toFieldInput.bind(this), true
    );

    this.sendButton.addEventListener(
      'click', this.onSendClick.bind(this)
    );

    this.container.addEventListener(
      'scroll', this.manageScroll.bind(this)
    );

    this.editHeader.addEventListener(
      'action', this.cancelEdit.bind(this)
    );

    this.headerText.addEventListener(
      'click', this.onHeaderActivation.bind(this)
    );

    // These events will be handled in handleEvent function
    this.container.addEventListener('click', this);
    this.container.addEventListener('contextmenu', this);
    this.editForm.addEventListener('submit', this);
    this.composeForm.addEventListener('submit', this);

    navigator.mozContacts.addEventListener(
      'contactchange',
      this.updateHeaderData.bind(this)
    );

    this.recipientSuggestions.addEventListener(
      'click',
      this.onRecipientSuggestionClick.bind(this)
    );

    MessageManager.on('message-sending', this.onMessageSending.bind(this));
    MessageManager.on('message-sent', this.onMessageSent.bind(this));
    MessageManager.on('message-received', this.onMessageReceived.bind(this));
    MessageManager.on(
      'message-failed-to-send',
      this.onMessageFailed.bind(this)
    );
    MessageManager.on('message-delivered', this.onDeliverySuccess.bind(this));
    MessageManager.on('message-read', this.onReadSuccess.bind(this));
    MessageManager.on('message-retrieving', this.onMessageRetrieving.bind(this));

    this.tmpl = templateIds.reduce(function(tmpls, name) {
      tmpls[Utils.camelCase(name)] =
        Template('messages-' + name + '-tmpl');
      return tmpls;
    }, {});

    Compose.init('messages-compose-form');

    // In case of input, we have to resize the input following UX Specs.
    Compose.on('input', this.messageComposerInputHandler.bind(this));
    Compose.on('subject-change', this.onSubjectChange.bind(this));
    Compose.on('segmentinfochange', this.onSegmentInfoChange.bind(this));

    // Assimilations
    // -------------------------------------------------
    // If the user manually types a recipient number
    // into the recipients list and does not "accept" it
    // via <ENTER> or ";", but proceeds to either
    // the message or attachment options, attempt to
    // gather those stranded recipients and assimilate them.
    //
    // Previously, an approach using the "blur" event on
    // the Recipients' "messages-to-field" element was used,
    // however the to-field will frequently lose "focus"
    // to any of its recipient children. If we assimilate on
    // to-field blur, the result is entirely unusable:
    //
    //  1. Focus will jump from the recipient input to the
    //      message input
    //  2. 1 or 2 characters may remain in the recipient
    //      editable, which will be "assimilated"
    //  3. If a user has made it past 1 & 2, any attempts to
    //      select a contact from contact search results
    //      will also jump focus to the message input field
    //
    // So we assimilate recipients if user starts to interact with Composer
    Compose.on('interact', this.assimilateRecipients.bind(this));

    this.container.addEventListener(
      'click', this.assimilateRecipients.bind(this)
    );

    this.timeouts.update = null;

    this.shouldChangePanelNextEvent = false;

    this.showErrorInFailedEvent = '';

    // Bound methods to be detachables
    this.onMessageTypeChange = this.onMessageTypeChange.bind(this);
  },

  onMessageInputFocusChange: function thui_onMessageInputFocusChange(e) {
    this.messagesComposeForm.classList.toggle('item-focus', e.type === 'focus');
  },

  onVisibilityChange: function thui_onVisibilityChange(e) {
    // If we leave the app and are in a thread or compose window
    // save a message draft if necessary
    if (document.hidden) {
      // Auto-save draft if the user has entered anything
      // in the composer or into To field (for composer panel only).
      var isAutoSaveRequired = false;

      if (Navigation.isCurrentPanel('thread')) {
        isAutoSaveRequired = !Compose.isEmpty();
      }

      if (isAutoSaveRequired) {
        ThreadUI.saveDraft({preserve: true, autoSave: true});
      }
    } else {
      this.recoveryFocus();
    }
  },

  // Recovery the focus when the page switch or node change.
  recoveryFocus: function thui_recoveryFocus() {
    var toFocus = document.querySelector('.focus');
    if (toFocus){
      toFocus.focus();
    } else {
      NavigationMap.setFocus('first');
    }
  },

  /**
   * Executes when the header 'action' button is tapped.
   *
   * @private
   */
  onHeaderAction: function thui_onHeaderAction() {
    this.backOrClose();
  },

  /**
   * We always go back to the previous pane, unless we're in an activity, then
   * in selected cases we exit the activity.
   *
   * @private
   */
  backOrClose: function thui_backOrClose() {
    var inActivity = ActivityHandler.isInActivity();
    var isComposer = Navigation.isCurrentPanel('composer');
    var isThread = Navigation.isCurrentPanel('thread');
    var action = inActivity && (isComposer || isThread) ? 'close' : 'back';
    this[action]();
  },

  // Initialize Recipients list and Recipients.View (DOM)
  initRecipients: function thui_initRecipients() {
    var recipientsChanged = (function recipientsChanged(length, record) {
      if (this.draft) {
        this.draft.isEdited = true;
      }
      var isOk = true;
      var strategy;

      if (record && (record.isQuestionable || record.isLookupable)) {
        if (record.isQuestionable) {
          isOk = false;
        }

        strategy = record.isLookupable ? 'searchContact' : 'exactContact';

        this[strategy](
          record.number, this.validateContact.bind(this, record)
        );
      }

      // Clean search result after recipient count change.
      this.toggleRecipientSuggestions();

      // The isOk flag will prevent "questionable" recipient entries from
      //
      //    - Updating the header
      //    - Enabling send.
      //
      //  Ideally, the contact will be found by the
      //  searchContact + validateContact operation and the
      //  handler will be re-called with a known
      //  and valid recipient from the user's contacts.
      if (isOk) {
        // update composer header whenever recipients change
        this.updateComposerHeader();

        this.emit('recipientschange');
      }
    }).bind(this);

    if (this.recipients) {
      this.recipients.length = 0;
      this.recipients.visible('singleline');
      this.recipients.focus();
    } else {
      this.recipients = new Recipients({
        outer: 'messages-to-field',
        inner: 'messages-recipients-list',
        template: this.tmpl.recipient
      });

      this.recipients.on('add', recipientsChanged);
      this.recipients.on('remove', recipientsChanged);
      this.recipients.on('modechange', function(mode) {
        this.threadMessages.classList.toggle(
          'multiline-recipients-mode',
           mode === 'multiline-mode'
        );
      }.bind(this));
    }
    this.hideToFieldHolder(false);
    this.toggleRecipientSuggestions();
  },

  initSentAudio: function thui_initSentAudio() {
    if (this.sentAudio) {
      return;
    }

    this.sentAudio = new Audio();
    this.sentAudio.preload = 'none';
    this.sentAudio.src = '/sounds/firefox_msg_sent.ogg';
    this.sentAudio.mozAudioChannelType = 'notification';

    // TODO move sentAudioEnabled management to Settings
    this.sentAudioKey = 'message.sent-sound.enabled';
    this.sentAudioEnabled = false;

    // navigator.mozSettings may not be defined in all environments
    if (navigator.mozSettings) {
      try {
        var req = navigator.mozSettings.createLock().get(this.sentAudioKey);
        req.onsuccess = (function onsuccess() {
          this.sentAudioEnabled = req.result[this.sentAudioKey];
        }).bind(this);

        navigator.mozSettings.addObserver(this.sentAudioKey, (function(e) {
          this.sentAudioEnabled = e.settingValue;
        }).bind(this));
      } catch (e) {
        this.sentAudioEnabled = false;
      }
    }
  },

  getAllInputs: function thui_getAllInputs() {
    if (this.container) {
      return Array.prototype.slice.call(
        this.container.querySelectorAll('input[type=checkbox]')
      );
    } else {
      return [];
    }
  },

  setHeaderAction: function thui_setHeaderAction(icon) {
    this.header.setAttribute('action', icon);
  },

  adjustScrollElement: function thui_adjustScrollElement() {
    if (Navigation.isCurrentPanel('thread')) {
      var items = document.querySelectorAll('ul.message-list li');
      var lastElement = items.item(items.length - 1);
      lastElement.scrollIntoView(false);
    }
  },

  messageComposerInputHandler: function thui_messageInputHandler(event) {
    if (Compose.type === 'sms') {
      this.hideMaxLengthNotice();
      this.adjustScrollElement();
      return;
    }

    if (Compose.isResizing) {
      this.resizeNotice.classList.remove('hide');

      if (this._resizeNoticeTimeout) {
        clearTimeout(this._resizeNoticeTimeout);
        this._resizeNoticeTimeout = null;
      }
    } else {
      this.checkMessageSize();
      if (this.resizeNotice.classList.contains('hide') ||
          this._resizeNoticeTimeout) {
        return;
      }

      this._resizeNoticeTimeout = setTimeout(function hideResizeNotice() {
        this.resizeNotice.classList.add('hide');
        this._resizeNoticeTimeout = null;
      }.bind(this), this.IMAGE_RESIZE_DURATION);
    }
  },

  showMaxLengthNotice: function thui_showMaxLengthNotice(opts) {
    Compose.lock();
    navigator.mozL10n.setAttributes(
      this.maxLengthNotice.querySelector('p'), opts.l10nId, opts.l10nArgs
    );
    this.maxLengthNotice.classList.remove('hide');
  },

  hideMaxLengthNotice: function thui_hideMaxLengthNotice() {
    Compose.unlock();
    this.maxLengthNotice.classList.add('hide');
  },

  showSubjectMaxLengthNotice: function thui_showSubjectMaxLengthNotice() {
    this.subjectMaxLengthNotice.classList.remove('hide');

    if (this.timeouts.subjectLengthNotice) {
      clearTimeout(this.timeouts.subjectLengthNotice);
    }
    this.timeouts.subjectLengthNotice = setTimeout(
      this.hideSubjectMaxLengthNotice.bind(this),
      this.BANNER_DURATION
    );
  },

  hideSubjectMaxLengthNotice: function thui_hideSubjectMaxLengthNotice() {
    this.subjectMaxLengthNotice.classList.add('hide');
    this.timeouts.subjectLengthNotice &&
      clearTimeout(this.timeouts.subjectLengthNotice);
  },

  /*
   * This function will be called before we slide to the thread or composer
   * panel. This way it can change things in the panel before the panel is
   * visible.
   */
  beforeEnter: function thui_beforeEnter(args) {
    this.clearConvertNoticeBanners();

    Recipients.View.isFocusable = true;
    this.initSentAudio();

    this.observeFocusChange_new();

    // XXX, workaround to fix a readout that will read placeholder in
    // message input when first enter Message. Set message input to hide,
    // and lter when enter thread ui, show it. We should check why placeholder
    // will be read later.
    this.messageInput.classList.remove('hide');

    this.currentMode = args.meta.next.panel;
    if (exports.option) {
      exports.option.hide();
    }
    switch (this.currentMode) {
      case 'composer':
        return this.beforeEnterComposer(args);
      case 'thread':
        return this.beforeEnterThread(args);
      default:
        console.error(
          'preEnter was called with an unexpected panel name:',
          this.currentMode
        );
        return Promise.reject();
    }
  },

  beforeEnterThread: function thui_beforeEnterThread(args) {
    // TODO should we implement hooks to Navigation so that Threads could
    // get an event whenever the panel changes?
    Threads.currentId = args.id;
    this.messageIdFromNotice = args.messageId;

    var prevPanel = args.meta.prev && args.meta.prev.panel;
    if (prevPanel !== 'group-view' && prevPanel !== 'report-view') {
      this.initializeRendering();
    }

    return this.updateHeaderData();
  },

  afterEnter: function thui_afterEnter(args) {
    window.addEventListener("backspace-long-press", this.longPressEvent);
    window.addEventListener("backspace-long-press-div", this.longPressEvent);
    window.addEventListener('keydown', this.handleKeyEvent);
    window.addEventListener('message-focusChanged', this.onFocusChanged);
    window.addEventListener('gaia-confirm-close', this.onFocusChanged);
    window.addEventListener('gaia-confirm-start-close', Utils.onDialogBeginClose);
    this.selector.subject.addEventListener('keydown', this.disableInputEnter);
    document.querySelector('#subject-composer-input').addEventListener('input', this.onFocusChanged);
    document.querySelector('#messages-input').addEventListener('keydown', this.handleInput);

    var next = args.meta.next.panel;
    this.EndKeySave = false;
    switch (next) {
      case 'composer':
        return this.afterEnterComposer(args);
      case 'thread':
        var prev = args.meta.prev;
        if(prev && prev.panel === "report-view"){
          this.updateSKs();
        }
        return this.afterEnterThread(args);
      default:
        console.error(
          'afterEnter was called with an unexpected panel name:',
          next
        );
        return Promise.reject();
    }
  },

  afterEnterComposer: function thui_afterEnterComposer(args) {
    // TODO Bug 1010223: should move to beforeEnter
    if (args.activity) {
      this.handleActivity(args.activity);
      NavigationMap.reset('thread-messages');

      var index = ThreadUI.getItemIndex(
        NavigationMap.getCurrentControl().elements, 'messages-input');
      NavigationMap.setFocus(index);
    } else if (args.forward) {
      this.handleForward(args.forward);
      NavigationMap.reset('thread-messages');
      setTimeout(() => {
        NavigationMap.setFocus(0);
      }, 400);
    } else if (this.draft || args.draftId || Threads.currentId) {
      // It would be nice to revisit these conditions in Bug 1010216.
      this.handleDraft(+args.draftId);
      NavigationMap.reset('thread-messages');
      setTimeout(() => {
        var index = ThreadUI.getItemIndex(NavigationMap.getCurrentControl().elements, 'messages-input');
        NavigationMap.setFocus(index);
      }, 400);
    } else {
      this.recipients.focus();
      NavigationMap.reset('thread-messages');
      setTimeout(() => {
        NavigationMap.setFocus(0);
      }, 400);
      ThreadUI.isMediaStorageExceed();
    }
    this.updateSKs(threadUiAddContact);

    this.enableConvertNoticeBanners();
    // not strictly necessary but better for consistency
    return Promise.resolve();
  },

  afterEnterThread: function thui_afterEnterThread(args) {
    var threadId = +args.id;
    this.currentThreadId = threadId;

    var prevPanel = args.meta.prev && args.meta.prev.panel;

    if (args.body) {
      this.messageInput.textContent = args.body;
      this.messageInput.setAttribute('data-placeholder', '');
    }

    if (prevPanel !== 'group-view' && prevPanel !== 'report-view') {
      this.renderMessages(threadId);

      // Populate draft if there is one
      // TODO merge with handleDraft ? Bug 1010216
      Drafts.request().then(() => {
        var thread = Threads.get(threadId);
        if (thread.hasDrafts) {
          this.draft = thread.drafts.latest;
          Compose.fromDraft(this.draft);
          this.draft.isEdited = false;
        } else {
          this.draft = null;
        }
        ThreadUI.enableConvertNoticeBanners();
      });
    }

    // Let's mark thread only when thread list is fully rendered and target node
    // is in the DOM tree.
    ThreadListUI.whenReady().then(function() {
      ThreadListUI.mark(threadId, 'read');
      // We need update the thread backup data when enter in thread.
      var threadData = Threads.get(threadId);
      if (threadData) {
        threadData.unreadCount = 0;
        Threads.set(threadId, threadData);
      }

      // We use setTimeout (macrotask) here to allow reflow happen as soon as
      // possible and to not interrupt it with non-critical task since Promise
      // callback only (microtask) won't help here.
      setTimeout(() => MessageManager.markThreadRead(threadId));
      ThreadUI.onFocusChanged();
    });

    return Utils.closeNotificationsForThread(threadId);
  },

  beforeLeave: function thui_beforeLeave(args) {
    window.removeEventListener('backspace-long-press', this.longPressEvent);
    window.removeEventListener('backspace-long-press-div', this.longPressEvent);
    window.removeEventListener('keydown', this.handleKeyEvent);
    window.removeEventListener('message-focusChanged', this.onFocusChanged);
    window.removeEventListener('gaia-confirm-close', this.onFocusChanged);
    window.removeEventListener('gaia-confirm-start-close', Utils.onDialogBeginClose);
    this.selector.subject.removeEventListener('keydown', this.disableInputEnter);
    document.querySelector('#subject-composer-input').removeEventListener('input', this.onFocusChanged);
    document.querySelector('#messages-input').removeEventListener('keydown', this.handleInput);
    this.stopContactNavigation();
    this.disableConvertNoticeBanners();

    var nextPanel = args.meta.next && args.meta.next.panel;

    // This should be in afterLeave, but the edit mode interface does not seem
    // to slide correctly. Bug 1009541
    this.cancelEdit();

    if (Navigation.isCurrentPanel('thread')) {
      // Revoke thumbnail URL for every image attachment rendered within thread
      var nodes = this.container.querySelectorAll(
        '.attachment-container[data-thumbnail]'
      );
      Array.from(nodes).forEach((node) => {
        window.URL.revokeObjectURL(node.dataset.thumbnail);
      });
    }

    // TODO move most of back() here: Bug 1010223
    if (nextPanel !== 'group-view' && nextPanel !== 'report-view') {
      this.cleanFields();
    }
    this.hideToFieldHolder(true);

    if (nextPanel !== 'report-view') {
      exports.option.hide();
    }
  },

  afterLeave: function thui_afterLeave(args) {
    Compose._removeSubjectFocusable();
    if (Navigation.isCurrentPanel('thread-list')) {
      this.container.textContent = '';
      this.cleanFields();
      Threads.currentId = null;
    }
    if (!Navigation.isCurrentPanel('composer')) {
      this.threadMessages.classList.remove('new');

      if (this.recipients) {
        this.recipients.length = 0;
      }

      this.toggleRecipientSuggestions();
    }
  },

  hideToFieldHolder: function(hide) {
    var placeHolder = document.querySelector('.recipient.navigable');
    if (placeHolder) {
      placeHolder.hidden = hide;
    }
  },

  handleForward: function thui_handleForward(forward) {
    var request = MessageManager.getMessage(+forward.messageId);

    request.onsuccess = (function() {
      Compose.fromMessage(request.result);

      Recipients.View.isFocusable = true;
      ThreadUI.recipients.focus();
    }).bind(this);

    request.onerror = function() {
      console.error('Error while forwarding:', this.error.name);
    };
  },

  handleActivity: function thui_handleActivity(activity) {
    /**
     * Choose the appropriate contact resolver:
     *  - if we have a phone number and no contact, rely on findByAddress
     *    to get a contact matching the number;
     *  - if we have a contact object and no phone number, just use a dummy
     *    source that returns the contact.
     */
    var findByAddress = Contacts.findByAddress.bind(Contacts);
    var number = activity.number;
    if (activity.contact && !number) {
      findByAddress = function dummySource(contact, cb) {
        cb(activity.contact);
      };
      number = activity.contact.number || activity.contact.tel[0].value;
    }

    // Add recipients and fill+focus the Compose area.
    if (activity.contact && number) {
      Utils.getContactDisplayInfo(
        findByAddress, number, function onData(data) {
          data.source = 'contacts';
          ThreadUI.recipients.add(data);
          Compose.fromMessage(activity);
        }
      );
    } else {
      if (number) {
        // If the activity delivered the number of an unknown recipient,
        // create a recipient directly.
        this.recipients.add({
          number: number,
          source: 'manual'
        });
      }
      Compose.fromMessage(activity);
    }


    if (number) {
      document.activeElement.blur();
      setTimeout(function () {
        Compose.focus();
      });
    } else {
      this.recipients.focus();
    }
  },

  // recalling draft for composer only
  // Bug 1010216 might use it for thread drafts too
  handleDraft: function thui_handleDraft(threadId) {
    // TODO Bug 1010216: remove this.draft
    var draft = this.draft ||
      Drafts.get(threadId || Threads.currentId);

    // Draft recipients are added as the composer launches
    if (draft) {
      // Recipients will exist for draft messages in threads
      // Otherwise find them from draft recipient numbers
      draft.recipients.forEach(function(number) {
        Contacts.findByAddress(number, function(records) {
          if (records.length) {
            this.recipients.add(
              Utils.basicContact(number, records[0])
            );
          } else {
            this.recipients.add({
              number: number
            });
          }
        }.bind(this));
      }, this);

      // Render draft contents into the composer input area.
      Compose.fromDraft(draft);

      // Discard this draft object and update the backing store
      Drafts.delete(draft).store();
      if (Settings.mmsEnable) {
        this.updateSKs(threadUiComposerOptions);
      } else {
        this.updateSKs(threadUiComposerOptions_noMMS);
      }
    } else {
      this.recipients.focus();
      this.updateSKs(threadUiAddContact);
    }

    if (this.draft) {
      this.draft.isEdited = false;
    }
  },

  beforeEnterComposer: function thui_beforeEnterComposer(args) {
    // TODO add the activity/forward/draft stuff here
    // instead of in afterEnter: Bug 1010223

    Threads.currentId = null;
    this.cleanFields();
    this.initRecipients();
    this.updateComposerHeader();
    this.container.textContent = '';
    this.threadMessages.classList.add('new');

    // not strictly necessary but being consistent
    return Promise.resolve();
  },

  assimilateRecipients: function thui_assimilateRecipients(args) {
    var isNew = Navigation.isCurrentPanel('composer');
    var node = this.recipientsList.lastChild;
    var typed;

    if (!isNew || node === null) {
      return;
    }

    // Ensure that Recipients does not trigger focus
    // on itself, which will cause the cursor to "jump"
    // back to the recipients input from the message input.
    Recipients.View.isFocusable = false;

    // Restore the recipients list input area to
    // single line view.
    this.recipients.visible('singleline');

    do {
      if (node.isPlaceholder) {
        typed = node.textContent.trim();

        // Clicking on the compose input will trigger
        // an assimilation. If the recipient input
        // is a lone semi-colon:
        //
        //  1. Clear the contents of the editable placeholder
        //  2. Do not assimilate the value.
        //
        if (typed === ';') {
          node.textContent = '';
          break;
        }

        // If the user actually typed something,
        // assume it's a manually entered recipient.
        // Push a recipient into the recipients
        // list with the left behind entry.
        if (typeof args == 'string') {
          if (args == 'false')
            node.textContent = '';
          break;
        }
        if (typed) {
          this.recipients.add({
            name: typed,
            number: typed,
            source: 'manual'
          });

          break;
        }
      }
    } while ((node = node.previousSibling))
  },

  // Function for handling when a new message (sent/received)
  // is detected
  onMessage: function onMessage(message) {
    // Update the stored thread data
    Threads.set(message.threadId, Thread.create(message));

    this.appendMessage(message);
    TimeHeaders.updateAll('header[data-time-update]');
  },

  isCurrentThread: function thui_isCurrentThread(threadId) {
    return Navigation.isCurrentPanel('thread', { id: threadId }) ||
      Navigation.isCurrentPanel('report-view', {
        threadId: threadId
      }) ||
      Navigation.isCurrentPanel('group-view', {
        id: threadId
      });
  },

  onMessageReceived: function thui_onMessageReceived(e) {
    var message = e.message;

    // If user currently in other thread then there is nothing to do here
    if (!this.isCurrentThread(message.threadId)) {
      return;
    }

    MessageManager.markMessagesRead([message.id]);

    this.onMessage(message);
    this.scrollViewToBottom();
    this.recoveryFocus();
  },

  onMessageSending: function thui_onMessageReceived(e) {
    var message = e.message;
    if (this.isCurrentThread(message.threadId)) {
      this.onMessage(message);
      this.forceScrollViewToBottom();
    } else {
      if (this.shouldChangePanelNextEvent) {
        Navigation.toPanel('thread', { id: message.threadId });
        this.shouldChangePanelNextEvent = false;
      }
    }
    Compose.focus();

    ThreadUI.dynamicSK();
  },

  /**
   * Fires once user clicks on any recipient in the suggestions list.
   */
  onRecipientSuggestionClick: function(event) {
    event.stopPropagation();
    event.preventDefault();

    // Since the "dataset" DOMStringMap property is essentially
    // just an object of properties that exactly match the properties
    // used for recipients, push the whole dataset object into
    // the current recipients list as a new entry.
    this.recipients.add(event.target.dataset).focus();
  },

  // Message composer type changed:
  onMessageTypeChange: function thui_onMessageTypeChange() {
    var messageUpperType = Compose.type + '-label';
    var tranlationType = navigator.mozL10n.get(messageUpperType);
    Toaster.showToast({
      messageL10nId: 'message-converted-to-toast',
      messageL10nArgs: {'type': tranlationType},
      latency: this.CONVERTED_MESSAGE_DURATION
    });
  },

  clearConvertNoticeBanners: function thui_clearConvertNoticeBanner() {
    this.convertNotice.classList.add('hide');
  },

  enableConvertNoticeBanners: function thui_enableConvertNoticeBanner() {
    Compose.on('type', this.onMessageTypeChange);
  },

  disableConvertNoticeBanners: function thui_disableConvertNoticeBanner() {
    Compose.off('type', this.onMessageTypeChange);
  },

  onSubjectChange: function thui_onSubjectChange() {
    if (Compose.isSubjectVisible && Compose.isSubjectMaxLength()) {
      this.showSubjectMaxLengthNotice();
    } else {
      this.hideSubjectMaxLengthNotice();
    }
  },

  // Triggered when the onscreen keyboard appears/disappears.
  resizeHandler: function thui_resizeHandler() {
    // Scroll to bottom
    this.scrollViewToBottom();
    // Make sure the caret in the "Compose" area is visible
    Compose.scrollMessageContent();
  },

  // Create a recipient from contacts activity.
  requestContact: function thui_requestContact() {
    if (typeof MozActivity === 'undefined') {
      console.log('MozActivity unavailable');
      return;
    }

    // Ensure that Recipients does not trigger focus on
    // itself, which causes the keyboard to appear.
    Recipients.View.isFocusable = false;
    var contactProperties = ['tel'];

    if (Settings.supportEmailRecipient) {
      contactProperties.push('email');
    }

    var activity = new MozActivity({
      name: 'pick',
      data: {
        type: 'webcontacts/select',
        contactProperties: contactProperties
      }
    });

    activity.onsuccess = (function() {
      if (!activity.result ||
          !activity.result.select ||
          !activity.result.select.length ||
          !activity.result.select[0].value) {
        console.error('The pick activity result is invalid.');
        return;
      }

      Recipients.View.isFocusable = true;

      var data = Utils.basicContact(
        activity.result.select[0].value, activity.result.contact
      );
      data.source = 'contacts';

      if (this.recipients.numbers.indexOf(activity.result.select[0].value) !== -1) {
        Toaster.showToast({
          messageL10nId: 'recipient-repeat',
          latency: 2000
        });
        return;
      }
      this.recipients.add(data);
    }).bind(this);

    activity.onerror = (function(e) {
      setTimeout( () => {
        Recipients.View.isFocusable = true;
        ThreadUI.recipients.focus();
      },300);
      console.log('WebActivities unavailable? : ' + e);
    }).bind(this);
  },

  // Method for updating the header when needed
  updateComposerHeader: function thui_updateComposerHeader() {
    var recipientCount = this.recipients.numbers.length;

    this.setHeaderContent('newMessage');
  },

  // We define an edge for showing the following chunk of elements
  manageScroll: function thui_manageScroll(oEvent) {
    if (this.isDownScroll) {
      this.isDownScroll = false;
      return;
    }
    var scrollTop = this.container.scrollTop;
    var scrollHeight = this.container.scrollHeight;
    var clientHeight = this.container.clientHeight;

    // kEdge will be the limit (in pixels) for showing the next chunk
    var kEdge = 30;
    if (scrollTop < kEdge) {
      this.showChunkOfMessages(this.CHUNK_SIZE);
      // We update the scroll to the previous position
      // taking into account the previous offset to top
      // and the current height due to we have added a new
      // chunk of visible messages
      this.container.scrollTop =
        (this.container.scrollHeight - scrollHeight) + scrollTop;
    }
  },

  scrollViewToBottom: function thui_scrollViewToBottom() {
    if (this.container.lastElementChild &&
        Navigation.isCurrentPanel('thread')) {
      this.container.lastElementChild.scrollIntoView(false);
    }
  },

  forceScrollViewToBottom: function thui_forceScrollViewToBottom() {
    this.scrollViewToBottom();
  },

  close: function thui_close() {
    return this._onNavigatingBack().then(() => {
      this.cleanFields();
      ActivityHandler.leaveActivity();
    }).catch(function(e) {
      // If we don't have any error that means that action was rejected
      // intentionally and there is nothing critical to report about.
      e && console.error('Unexpected error while closing the activity: ', e);

      return Promise.reject(e);
    });
  },

  back: function thui_back(close) {
    if (Navigation.isCurrentPanel('group-view') ||
        Navigation.isCurrentPanel('report-view')) {
      Navigation.toPanel('thread', { id: Threads.currentId });
      this.updateHeaderData();

      return Promise.resolve();
    }

    return this._onNavigatingBack(close).then(function() {
      if (ActivityHandler.isInActivity() && !ActivityHandler._lastMessage) {
        ActivityHandler.leaveActivity();
      } else {
        if (close) {
          window.close();
        } else {
          var focus = document.querySelector('.focus');
          focus && focus.classList.remove('focus');
          Navigation.toPanel('thread-list');
        }
      }
    }.bind(this)).catch(function(e) {
      e && console.error('Unexpected error while navigating back: ', e);

      return Promise.reject(e);
    });
  },

  _onNavigatingBack: function(close) {
    this.stopRendering();

    // We're waiting for the keyboard to disappear before animating back
    return this._ensureKeyboardIsHidden().then(function() {
      // Need to assimilate recipients in order to check if any entered
      this.assimilateRecipients('true');

      // If we're leaving a thread's message view,
      // ensure that the thread object's unreadCount
      // value is current (set = 0)
      if (Threads.active) {
        Threads.active.unreadCount = 0;
      }

      // If the composer is empty, we
      // do not prompt to save a draft and discard drafts
      // as the user deleted them manually
      if (Compose.isEmpty()) {
        this.discardDraft();
        var node = this.recipientsList.lastChild;
        node.textContent = '';
        if (node.classList && node.classList.contains('focus')) {
          node.classList.remove('focus');
        }
        return;
      }

      // If there is a draft and the content and recipients
      // never got edited, re-save if threadless,
      // then leave without prompting to replace
      if (this.draft && !this.draft.isEdited) {
        // Thread-less drafts are orphaned at this point
        // so they need to be resaved for persistence
        // Otherwise, clear the draft directly before leaving
        if (!Threads.currentId) {
          this.saveDraft({autoSave: true});
        } else {
          this.draft = null;
        }
        return;
      }

      return this._showMessageSaveOrDiscardPrompt(close);
    }.bind(this));
  },

  isKeyboardDisplayed: function thui_isKeyboardDisplayed() {
    /* XXX: Detect if the keyboard is visible. The keyboard minimal height is
     * 150px; when in reduced attention screen mode however the difference
     * between window height and the screen height will be larger than 150px
     * thus correctly yielding false here. */
    return ((window.screen.height - window.innerHeight) > 150);
  },

  _ensureKeyboardIsHidden: function() {
    if (this.isKeyboardDisplayed()) {
      return new Promise(function(resolve) {
        var setTimer = window.setTimeout(resolve, 400);
        window.addEventListener('resize', function keyboardHidden() {
          window.clearTimeout(setTimer);
          window.removeEventListener('resize', keyboardHidden);
          resolve();
        });
      });
    }
    return Promise.resolve();
  },

  _showMessageSaveOrDiscardPrompt: function(close) {
    if (option.menuVisible && close) {
      option.menu.hide();
    }
    return new Promise(function(resolve, reject) {
      Utils.confirmAlert('confirmation-title', 'draft-save-content',
                         'cancel', cancelCallback, 'save-as-draft',
                         acceptCallback, 'discard-message', confirmCallback);
      function cancelCallback() {
        reject();
      }
      function acceptCallback() {
        ThreadUI.saveDraft();
        resolve();
      }
      function confirmCallback() {
        ThreadUI.assimilateRecipients('false');
        ThreadUI.discardDraft();
        resolve();
      }
    }.bind(this));
  },

  onSegmentInfoChange: function thui_onSegmentInfoChange() {
    var currentSegment = Compose.segmentInfo.segments;

    var isValidSegment = currentSegment > 0;
    var isSegmentChanged = this.previousSegment !== currentSegment;
    var isStartingFirstSegment = this.previousSegment === 0 &&
          currentSegment === 1;

    if (Compose.type === 'sms' && isValidSegment && isSegmentChanged &&
        !isStartingFirstSegment) {
      this.previousSegment = currentSegment;

      navigator.mozL10n.setAttributes(
        this.smsCounterNotice.querySelector('p'),
        'sms-counter-notice-label',
        { number: currentSegment }
      );
      this.smsCounterNotice.classList.remove('hide');
      window.setTimeout(function() {
        this.smsCounterNotice.classList.add('hide');
      }.bind(this), this.ANOTHER_SMS_TOAST_DURATION);
    }
  },

  checkMessageSize: function thui_checkMessageSize() {
    // Counter should be updated when image resizing complete
    if (Compose.isResizing) {
      return false;
    }

    if (Settings.mmsSizeLimitation) {
      if (Compose.size > Settings.mmsSizeLimitation) {
        Utils.confirmAlert('attention',
          {
            id: 'multimedia-message-exceeded-max-length',
            args: { mmsSize: (Settings.mmsSizeLimitation / 1024 / 1024).toFixed(2) }
          },
          null, null, 'ok', null, null, null);
        return false;
      } else if (Compose.size === Settings.mmsSizeLimitation) {
        this.showMaxLengthNotice({ l10nId: 'messages-max-length-text' });
        return true;
      }
    }

    this.hideMaxLengthNotice();
    return true;
  },

  // Adds a new grouping header if necessary (today, tomorrow, ...)
  getMessageContainer:
    function thui_getMessageContainer(messageTimestamp, hidden) {
    var startOfDayTimestamp = Utils.getDayDate(messageTimestamp);
    var messageDateGroup = document.getElementById('mc_' + startOfDayTimestamp);

    var header,
        messageContainer;

    if (messageDateGroup) {
      header = messageDateGroup.firstElementChild;
      messageContainer = messageDateGroup.lastElementChild;

      if (messageTimestamp < header.dataset.time) {
        header.dataset.time = messageTimestamp;
      }
      return messageContainer;
    }

    // If there is no messageContainer we have to create it
    messageDateGroup = this.tmpl.dateGroup.prepare({
      id: 'mc_' + startOfDayTimestamp,
      timestamp: startOfDayTimestamp.toString(),
      headerTimeUpdate: 'repeat',
      headerTime: messageTimestamp.toString(),
      headerDateOnly: 'true'
    }).toDocumentFragment().firstElementChild;

    header = messageDateGroup.firstElementChild;
    messageContainer = messageDateGroup.lastElementChild;

    if (hidden) {
      header.classList.add('hidden');
    } else {
      TimeHeaders.update(header);
    }

    this._insertTimestampedNodeToContainer(messageDateGroup, this.container);

    return messageContainer;
  },

  // Method for updating the header with the info retrieved from Contacts API
  updateHeaderData: function thui_updateHeaderData() {
    var thread, number;

    thread = Threads.active;

    if (!thread) {
      return Promise.resolve();
    }

    number = thread.participants[0];

    // Add data to contact activity interaction
    this.headerText.dataset.number = number;

    return new Promise(function(resolve, reject) {
      Contacts.findByAddress(number, function gotContact(contacts) {
        // For the basic display, we only need the first contact's information
        // e.g. for 3 contacts, the app displays:
        //
        //    Jane Doe (+2)
        //
        var details = Utils.getContactDetails(number, contacts, {photoURL: true});
        // Bug 867948: contacts null is a legitimate case, and
        // getContactDetails is okay with that.
        var contactName = details.title || number;
        this.headerText.dataset.isContact = !!details.isContact;
        this.headerText.dataset.title = contactName;

        var headerContentTemplate = thread.participants.length > 1 ?
          this.tmpl.groupHeader : this.tmpl.header;
        this.setHeaderContent({
          html: headerContentTemplate.interpolate({
            name: contactName,
            participantCount: (thread.participants.length - 1).toString()
          })
        });
        resolve();
      }.bind(this));
    }.bind(this));
  },

  /**
   * Updates header content since it's used for different panels and should be
   * carefully handled for every case. In Thread panel header contains HTML
   * markup to support bidirectional content, but other panels still use it with
   * mozL10n.setAttributes as it would contain only localizable text. We should
   * get rid of this method once bug 961572 and bug 1011085 are landed.
   * @param {string|{ html: string }|{id: string, args: Object }} contentL10n
   * Should be either safe HTML string or l10n properties.
   * @public
   */
  setHeaderContent: function thui_setHeaderContent(contentL10n) {
    if (typeof contentL10n === 'string') {
      contentL10n = { id: contentL10n };
    }

    if (contentL10n.id) {
      // Remove rich HTML content before we set l10n attributes as l10n lib
      // fails in this case
      this.headerText.firstElementChild && (this.headerText.textContent = '');
      navigator.mozL10n.setAttributes(
        this.headerText, contentL10n.id, contentL10n.args
      );
      return;
    }

    if (contentL10n.html) {
      this.headerText.removeAttribute('data-l10n-id');
      this.headerText.innerHTML = contentL10n.html;
      return;
    }
  },

  initializeRendering: function thui_initializeRendering() {
    // Clean fields
    this.cleanFields();

    // Clean list of messages
    this.container.innerHTML = '';
    // Initialize infinite scroll params
    this.messageIndex = 0;
    // reset stopRendering boolean
    this._stopRenderingNextStep = false;
    this._renderingMessage = true;
  },

  // Method for stopping the rendering when clicking back
  stopRendering: function thui_stopRendering() {
    this._stopRenderingNextStep = true;
    this._renderingMessage = false;
  },

  // Method for rendering the first chunk at the beginning
  showFirstChunk: function thui_showFirstChunk() {
    // Show chunk of messages
    this.showChunkOfMessages(this.CHUNK_SIZE);
    // Boot update of headers
    TimeHeaders.updateAll('header[data-time-update]');
    // Go to Bottom
    this.scrollViewToBottom();

    NavigationMap.reset('thread-messages');
    var focusId = null;

    if (ThreadListUI.isSwitchCase || ActivityHandler._lastMessage) {
      var lastNodes = document.querySelectorAll('.message');
      if (lastNodes.length !== 0) {
        var lastId = lastNodes[lastNodes.length - 1].id;
        this.messageIdFromNotice = lastId.replace('message-', '');
        ThreadListUI.isSwitchCase = false;
      }
    }

    if (this.messageIdFromNotice) {
      focusId = 'message-' + this.messageIdFromNotice;
    } else {
      focusId = 'messages-input';
    }
    var index = ThreadUI.getItemIndex(NavigationMap.getCurrentControl().elements, focusId);
    NavigationMap.setFocus(index);
  },

  createMmsContent: function thui_createMmsContent(dataArray) {
    var container = document.createDocumentFragment();

    dataArray.forEach(function(messageData) {

      if (messageData.blob) {
        var attachment = new Attachment(messageData.blob, {
          name: messageData.name
        });
        var mediaElement = attachment.render();
        container.appendChild(mediaElement);
        attachmentMap.set(mediaElement, attachment);
      }

      if (messageData.text) {
        var textElement = document.createElement('span');

        // escape text for html and look for clickable numbers, etc.
        var text = Template.escape(messageData.text);
        text = LinkHelper.searchAndLinkClickableData(text);

        textElement.innerHTML = text;
        container.appendChild(textElement);
      }
    });
    return container;
  },

  // Method for rendering the list of messages using infinite scroll
  renderMessages: function thui_renderMessages(threadId, callback) {
    // Use taskRunner to make sure message appended in proper order
    var taskQueue = new TaskRunner();
    this.threadIdBackUp = threadId;

    var onMessagesRendered = (function messagesRendered() {
      if (this.messageIndex < this.CHUNK_SIZE) {
        taskQueue.push(this.showFirstChunk.bind(this));
      }

      if (callback) {
        callback();
      }
      this._renderingMessage = false;
      window.dispatchEvent(new CustomEvent('messages-render-complete'));
    }).bind(this);

    var onRenderMessage = (function renderMessage(message) {
      if (this.threadIdBackUp && (this.threadIdBackUp !== message.threadId)) {
        return false;
      }

      if (this._stopRenderingNextStep) {
        // stop the iteration and clear the taskQueue
        taskQueue = null;
        return false;
      }
      taskQueue.push(() => {
        if (!this._stopRenderingNextStep) {
          return this.appendMessage(message,/*hidden*/ true);
        }
        return false;
      });
      this.messageIndex++;
      if (this.messageIndex === this.CHUNK_SIZE) {
        taskQueue.push(this.showFirstChunk.bind(this));
      }
      return true;
    }).bind(this);

    if (this._stopRenderingNextStep) {
      // we were already asked to stop rendering, before even starting
      return;
    }

    var filter = { threadId: threadId };

    // We call getMessages with callbacks
    var renderingOptions = {
      each: onRenderMessage,
      filter: filter,
      invert: false,
      end: onMessagesRendered,
      chunkSize: this.CHUNK_SIZE
    };

    MessageManager.getMessages(renderingOptions);
  },

  // generates the html for not-downloaded messages - pushes class names into
  // the classNames array also passed in, returns an HTML string
  _createNotDownloadedHTML:
  function thui_createNotDownloadedHTML(message, classNames) {
    // default strings:
    var messageL10nId = 'tobedownloaded-attachment';
    var downloadL10nId = 'download-attachment';

    // assuming that incoming message only has one deliveryInfo
    var status = message.deliveryInfo[0].deliveryStatus;

    var expireFormatted = Utils.date.format.localeFormat(
      new Date(+message.expiryDate), navigator.mozL10n.get('expiry-date-format')
    );

    var expired = +message.expiryDate < Date.now();

    if (expired) {
      classNames.push('expired');
      messageL10nId = 'expired-attachment';
    }

    if (status === 'error') {
      classNames.push('error');
    }

    if (status === 'pending') {
      downloadL10nId = 'downloading-attachment';
      classNames.push('pending');
    }

    return this.tmpl.notDownloaded.interpolate({
      messageL10nId: messageL10nId,
      messageL10nArgs: JSON.stringify({ date: expireFormatted }),
      messageL10nDate: message.expiryDate.toString(),
      messageL10nDateFormat: 'expiry-date-format',
      downloadL10nId: downloadL10nId
    });
  },

  // Check deliveryStatus for both single and multiple recipient case.
  // In multiple recipient case, we return true only when all the recipients
  // deliveryStatus set to success.
  shouldShowDeliveryStatus: function thui_shouldShowDeliveryStatus(message) {
    if (message.delivery !== 'sent') {
      return false;
    }

    if (message.type === 'mms') {
      return message.deliveryInfo.every(function(info) {
        return info.deliveryStatus === 'success';
      });
    } else {
      return message.deliveryStatus === 'success';
    }
  },

  // Check readStatus for both single and multiple recipient case.
  // In multiple recipient case, we return true only when all the recipients
  // deliveryStatus set to success.
  shouldShowReadStatus: function thui_shouldShowReadStatus(message) {
    // Only mms message has readStatus
    if (message.delivery !== 'sent' || message.type === 'sms' ||
      !message.deliveryInfo) {
      return false;
    }

    return message.deliveryInfo.every(function(info) {
      return info.readStatus === 'success';
    });
  },

  buildMessageDOM: function thui_buildMessageDOM(message, hidden) {
    var messageDOM = document.createElement('li'),
        bodyHTML = '';

    var messageStatus = message.delivery,
        isNotDownloaded = messageStatus === 'not-downloaded',
        isIncoming = messageStatus === 'received' || isNotDownloaded;

    // If the MMS has invalid empty content(message without attachment and
    // subject) or contains only subject, we will display corresponding message
    // and layout type in the message bubble.
    //
    // Returning attachments would be different based on gecko version:
    // null in b2g18 / empty array in master.
    var noAttachment = (message.type === 'mms' && !isNotDownloaded &&
      (message.attachments === null || message.attachments.length === 0));
    var invalidEmptyContent = (noAttachment && !message.subject);

    if (this.shouldShowReadStatus(message)) {
      messageStatus = 'read';
    } else if (this.shouldShowDeliveryStatus(message)) {
      messageStatus = 'delivered';
    }

    // If compose -> thread and switching ril,
    // the message status will be sending.
    if (this.switchFlag && (messageStatus === 'error')) {
      messageStatus = 'sending';
      this.switchFlag = false;
    }

    var classNames = [
      'navigable', 'message', message.type, messageStatus,
      isIncoming ? 'incoming' : 'outgoing'
    ];

    if (hidden) {
      classNames.push('hidden');
    }

    if (message.type && message.type === 'mms' && message.subject) {
      classNames.push('has-subject');
    }

    if (message.type && message.type === 'sms') {
      var escapedBody = Template.escape(message.body || '');
      bodyHTML = LinkHelper.searchAndLinkClickableData(escapedBody);
    }

    if (isNotDownloaded) {
      bodyHTML = this._createNotDownloadedHTML(message, classNames);
    }

    if (invalidEmptyContent) {
      classNames = classNames.concat(['error', 'invalid-empty-content']);
    } else if (noAttachment) {
      classNames.push('no-attachment');
    }

    if (classNames.indexOf('error') >= 0) {
      messageStatus = 'error';
    } else if (classNames.indexOf('pending') >= 0) {
      messageStatus = 'pending';
    }

    messageDOM.className = classNames.join(' ');
    messageDOM.id = 'message-' + message.id;
    messageDOM.dataset.messageId = message.id;
    messageDOM.dataset.iccId = message.iccId;
    messageDOM.setAttribute('role', 'menuitem');
    var simServiceId = Settings.getServiceIdByIccId(message.iccId);
    var showSimInformation = Settings.hasSeveralSim() && simServiceId !== null;
    var simInformationHTML = '';
    // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name begin
    var simName = '';
    if (simServiceId == 0) {
      simName = Settings.sim1Name ? Settings.sim1Name : 'SIM 1';
    } else {
      simName = Settings.sim2Name ? Settings.sim2Name : 'SIM 2';
    }
    if (showSimInformation) {
      simInformationHTML = this.tmpl.messageSimInformation.interpolate({
        simNumberL10nArgs: JSON.stringify({ id: simName })
      });
    }
    // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name end

    messageDOM.innerHTML = this.tmpl.message.interpolate({
      id: String(message.id),
      bodyHTML: bodyHTML,
      timestamp: String(message.timestamp),
      subject: String(message.subject),
      simInformationHTML: simInformationHTML,
      messageStatusHTML: this.getMessageStatusMarkup(messageStatus).toString()
    }, {
      safe: ['bodyHTML', 'simInformationHTML', 'messageStatusHTML']
    });

    TimeHeaders.update(messageDOM.querySelector('time'));

    messageDOM.querySelector('.deliver-mark').classList
      .toggle('hide', (messageStatus === 'delivered' ? false : true));

    var pElement = messageDOM.querySelector('p');
    if (invalidEmptyContent) {
      pElement.setAttribute('data-l10n-id', 'no-attachment-text');
    }

    if (message.type === 'mms' && !isNotDownloaded && !noAttachment) { // MMS
      return this.mmsContentParser(message).then((mmsContent) => {
        pElement.appendChild(mmsContent);
        return messageDOM;
      });
    }

    return Promise.resolve(messageDOM);
  },

  mmsContentParser: function thui_mmsContentParser(message) {
    return new Promise((resolver) => {
      SMIL.parse(message, (slideArray) => {
        resolver(this.createMmsContent(slideArray));
      });
    });
  },

  getMessageStatusMarkup: function(status) {
    return ['read', 'sending', 'error'].indexOf(status) >= 0 ?
      this.tmpl.messageStatus.prepare({
        statusL10nId: 'message-delivery-status-' + status
      }) : '';
  },

  appendMessage: function thui_appendMessage(message, hidden) {
    var timestamp = +message.timestamp;

    // look for an old message and remove it first - prevent anything from ever
    // double rendering for now
    var messageDOM = document.getElementById('message-' + message.id);

    if (messageDOM) {
      this.removeMessageDOM(messageDOM);
    }

    // build messageDOM adding the links
    return this.buildMessageDOM(message, hidden).then((messageDOM) => {
      if (this._stopRenderingNextStep) {
        return;
      }

      messageDOM.dataset.timestamp = timestamp;

      // Add to the right position
      var messageContainer = this.getMessageContainer(timestamp, hidden);
      this._insertTimestampedNodeToContainer(messageDOM, messageContainer);

      if (this.inEditMode) {
        this.checkInputs();
      }

      if (!hidden) {
        // Go to Bottom
        this.scrollViewToBottom();
      }

      window.dispatchEvent(new CustomEvent('message-dom-created'));
    });
  },

  /**
   * Inserts DOM node to the container respecting 'timestamp' data attribute of
   * the node to insert and sibling nodes in ascending order.
   * @param {Node} nodeToInsert DOM node to insert.
   * @param {Node} container Container DOM node to insert to.
   * @private
   */
  _insertTimestampedNodeToContainer: function(nodeToInsert, container) {
    var currentNode = container.firstElementChild,
        nodeTimestamp = nodeToInsert.dataset.timestamp;

    while (currentNode && nodeTimestamp > +currentNode.dataset.timestamp) {
      currentNode = currentNode.nextElementSibling;
    }

    // With this function, "inserting before 'null'" means "appending"
    container.insertBefore(nodeToInsert, currentNode || null);
  },

  showChunkOfMessages: function thui_showChunkOfMessages(number, position) {
    var elements = this.container.querySelectorAll('.hidden');
    if (position === 'ArrowDown') {
      elements = Array.slice(elements, 0).slice(0, number);
      ThreadUI.isDownScroll = true;
    } else {
      elements = Array.slice(elements, -number);
    }

    elements.forEach((element) => {
      element.classList.remove('hidden');
      if (element.tagName === 'HEADER') {
        TimeHeaders.update(element);
      }
    });
  },

  showOptions: function thui_showOptions() {
    /**
      * Different situations depending on the state
      * - 'Add Subject' if there's none, 'Delete subject' if already added
      * - 'Delete messages' for existing conversations
      */
    var params = {
      header: { l10nId: 'options' },
      items: []
    };

    // Subject management
    var subjectItem;
    if (Compose.isSubjectVisible) {
      subjectItem = {
        l10nId: 'remove-subject',
        method: Compose.hideSubject
      };
    } else {
      subjectItem = {
        l10nId: 'add-subject',
        method: Compose.showSubject
      };
    }
    params.items.push(subjectItem);

    // If we are on a thread, we can call to SelectMessages
    if (Navigation.isCurrentPanel('thread')) {
      params.items.push({
        l10nId: 'selectMessages-label',
        method: this.startEdit.bind(this)
      });
    }

    // Last item is the Cancel button
    params.items.push({
      l10nId: 'cancel',
      incomplete: true
    });

    new OptionMenu(params).show();
  },

  startEdit: function thui_edit() {

    function editModeSetup() {
      /*jshint validthis:true */
      this.inEditMode = true;
      this.selectionHandler.cleanForm();
      this.mainWrapper.classList.toggle('edit');
    }

    if (!this.selectionHandler) {
      LazyLoader.load('js/selection_handler.js', () => {
        this.selectionHandler = new SelectionHandler({
          // Elements
          container: this.container,
          checkUncheckAllButton: this.checkUncheckAllButton,
          // Methods
          checkInputs: this.checkInputs.bind(this),
          getAllInputs: this.getAllInputs.bind(this),
          isInEditMode: this.isInEditMode.bind(this),
          updateSKs: this.updateSKs.bind(this)
        });
        editModeSetup.call(this);
      });
    } else {
      editModeSetup.call(this);
    }
  },

  clickCheckUncheckAllButton: function() {
    this.checkUncheckAllButton.click();
  },

  isInEditMode: function thui_isInEditMode() {
    return this.inEditMode;
  },

  deleteUIMessages: function thui_deleteUIMessages(list, callback) {
    // Strategy:
    // - Delete message/s from the DOM
    // - Update the thread in thread-list without re-rendering
    // the entire list
    // - move to thread list if needed

    if (!Array.isArray(list)) {
      list = [list];
    }

    // Removing from DOM all messages to delete
    for (var i = 0, l = list.length; i < l; i++) {
      ThreadUI.removeMessageDOM(
        document.getElementById('message-' + list[i])
      );
    }

    callback = typeof callback === 'function' ? callback : function() {};

    // Do we remove all messages of the Thread?
    if (!ThreadUI.container.firstElementChild) {
      // Remove the thread from DOM and go back to the thread-list
      ThreadListUI.removeThread(Threads.currentId);
      callback();
      this.backOrClose();
    } else {
      // Retrieve latest message in the UI
      var lastMessage = ThreadUI.container.lastElementChild.querySelector(
        'li:last-child'
      );
      // We need to make Thread-list to show the same info
      var request = MessageManager.getMessage(+lastMessage.dataset.messageId);
      request.onsuccess = function() {
        callback();
        ThreadListUI.updateThread(request.result, { deleted: true });
      };
      request.onerror = function() {
        console.error('Error when updating the list of threads');
        callback();
      };
    }
  },

  delete: function thui_delete() {
    function performDeletion() {
      /* jshint validthis: true */

      WaitingScreen.show();
      if (exports.option) {
        exports.option.hide();
      }
      var items = ThreadUI.selectionHandler.selectedList;
      var delNumList = items.map(item => +item);

      // Complete deletion in DB and in UI
      MessageManager.deleteMessages(delNumList,
          function onDeletionDone() {
            ThreadUI.deleteUIMessages(delNumList, function uiDeletionDone() {
              ThreadUI.cancelEdit();
              WaitingScreen.hide();
              if (Settings.mmsEnable) {
                ThreadUI.updateSKs(threadUiNormalOptions);
              } else {
                ThreadUI.updateSKs(threadUiNormalOptions_noMMS);
              }
              ThreadUI.showToaster('editMode', delNumList.length);
              NavigationMap.reset('thread-messages');
              var index = ThreadUI.getItemIndex(NavigationMap.getCurrentControl().elements, 'messages-input');
              NavigationMap.setFocus(index);
            });
          }
      );
    }

    Utils.confirmAlert('confirmation-title', 'deleteMessages-confirmation',
                       'cancel', null, null, null, 'delete', deleteCallback);
    function deleteCallback() {
      performDeletion();
    }
  },

  cancelEdit: function thlui_cancelEdit() {
    if (this.inEditMode) {
      this.inEditMode = false;
      this.mainWrapper.classList.remove('edit');
    }
  },

  checkInputs: function thui_checkInputs() {
    var selected = this.selectionHandler.selectedCount;
    var allInputs = this.allInputs;

    var isAnySelected = selected > 0;

    // Manage buttons enabled\disabled state
    if (selected === allInputs.length) {
      this.checkUncheckAllButton.setAttribute('data-l10n-id', 'deselect-all');
    } else {
      this.checkUncheckAllButton.setAttribute('data-l10n-id', 'select-all');
    }

    if (isAnySelected) {
      navigator.mozL10n.setAttributes(this.editMode, 'selected-messages',
        {n: selected});
    } else {
      navigator.mozL10n.setAttributes(this.editMode, 'deleteMessages-title');
    }
    navigator.mozL10n.setAttributes(this.editMode, 'deleteMessages-title');
    navigator.mozL10n.setAttributes(this.editMode, 'selected-messages',{n: selected});
  },

  handleMessageClick: function thui_handleMessageClick(evt) {
    var currentNode = evt.target;
    var elems = {};

    // Walk up the DOM, inspecting all the elements
    while (currentNode && currentNode.classList) {
      if (currentNode.classList.contains('bubble')) {
        elems.bubble = currentNode;
      } else if (currentNode.classList.contains('message')) {
        elems.message = currentNode;
      } else if (currentNode.classList.contains('message-status')) {
        elems.messageStatus = currentNode;
      }
      currentNode = currentNode.parentNode;
    }

    // Click event handlers that occur outside of a message element should be
    // defined elsewhere.
    if (!(elems.message && elems.bubble)) {
      return;
    }

    // handle not-downloaded messages
    if (elems.message.classList.contains('not-downloaded')) {

      // do nothing for pending downloads, or expired downloads
      if (elems.message.classList.contains('expired') ||
        elems.message.classList.contains('pending')) {
        return;
      }
      this.retrieveMMS(elems.message);
      return;
    }

    // Do nothing for invalid empty content error because it's not possible to
    // retrieve message again in this edge case.
    if (elems.message.classList.contains('invalid-empty-content')) {
      return;
    }

    // Click events originating from a "message-status" aside of an error
    // message should trigger a prompt for retransmission.
    if (elems.message.classList.contains('error') && elems.messageStatus) {
      Utils.confirm('resend-confirmation').then(() => {
        this.resendMessage(elems.message.dataset.messageId);
      });
    }
  },

  /*
   * Given an element of a message, this function will dive into
   * the DOM for getting the bubble container of this message.
   */
  getMessageBubble: function thui_getMessageContainer(element) {
    var node = element;
    var bubble;

    do {
      if (node.classList.contains('bubble')) {
        bubble = node;
      }

      // If we have a bubble and we reach the li with dataset.messageId
      if (bubble) {
        if (node.dataset && node.dataset.messageId) {
          return {
            id: +node.dataset.messageId,
            node: bubble
          };
        }
      }

      // If we reach the container, quit.
      if (node.id === 'thread-messages') {
        return null;
      }
    } while ((node = node.parentNode));

    return null;
  },

  handleEvent: function thui_handleEvent(evt) {
    switch (evt.type) {
      case 'click':
        if (this.inEditMode) {
          return;
        }

        // if the click wasn't on an attachment check for other clicks
        if (!attachmentMap.get(evt.target)) {
          this.handleMessageClick(evt);
        }
        return;
      case 'contextmenu':
        evt.preventDefault();
        evt.stopPropagation();

        var messageBubble = this.getMessageBubble(evt.target);

        if (!messageBubble) {
          return;
        }
        var lineClassList = messageBubble.node.parentNode.classList;

        // Show options per single message
        var messageId = messageBubble.id;
        var params = {
          type: 'action',
          header: { l10nId: 'options' },
          items:[]
        };

        if (lineClassList && !lineClassList.contains('not-downloaded')) {
          params.items.push({
            l10nId: 'forward',
            method: function forwardMessage(messageId) {
              Navigation.toPanel('composer', {
                forward: {
                  messageId: messageId
                }
              });
            },
            params: [messageId]
          });
        }

        params.items.push(
          {
            l10nId: 'select-text',
            method: (node) => {
              this.enableBubbleSelection(
                node.querySelector('.message-content-body')
              );
            },
            params: [messageBubble.node]
          },
          {
            l10nId: 'view-message-report',
            method: function showMessageReport(messageId) {
              // Fetch the message by id for displaying corresponding message
              // report. threadId here is to make sure thread is updatable
              // when current view report panel.
              Navigation.toPanel('report-view', {
                id: messageId,
                threadId: Threads.currentId
              });
            },
            params: [messageId]
          },
          {
            l10nId: 'delete',
            method: function deleteMessage(messageId) {
              Utils.confirm(
                'deleteMessage-confirmation', null,
                { text: 'delete', className: 'danger' }
              ).then(() => {
                MessageManager.deleteMessages(
                  messageId, () => ThreadUI.deleteUIMessages(messageId)
                );
              });
            },
            params: [messageId]
          }
        );

        if (lineClassList && lineClassList.contains('error') &&
            lineClassList.contains('outgoing')) {
          params.items.push({
            l10nId: 'resend-message',
            method: this.resendMessage.bind(this, messageId),
            params: [messageId]
          });
        }

        params.items.push({
          l10nId: 'cancel'
        });

        var options = new OptionMenu(params);
        options.show();

        break;
      case 'submit':
        evt.preventDefault();
        break;
    }
  },

  cleanFields: function thui_cleanFields() {
    this.previousSegment = 0;

    if (this.recipients) {
      this.recipients.length = 0;
    }

    Compose.clear();
  },

  onSendClick: function thui_onSendClick() {
    if (Compose.isEmpty()) {
      return;
    }

    // Assimilation 3 (see "Assimilations" above)
    // User may return to recipients, type a new recipient
    // manually and then click the sendButton without "accepting"
    // the recipient.
    this.assimilateRecipients();

    // not sure why this happens - replace me if you know
    this.container.classList.remove('hide');
  },

  switchThreadSelection: function thui_switchThreadSelection(callback) {
    function cancelCallback() {
      ThreadUI.confirmDialogShown = false;
    }
    function acceptCallback() {
      ThreadUI.confirmDialogShown = false;
      callback();
    }
    function confirmCallback() {
      ThreadUI.confirmDialogShown = false;
      Compose.clear();
      callback();
    }
    Utils.confirmAlert('confirmation-title', 'draft-save-content',
      'cancel', cancelCallback, 'save-as-draft',
      acceptCallback, 'discard-message', confirmCallback);
    ThreadUI.confirmDialogShown = true;
  },

  handleKeyEvent: function thui_handleKeyEvent(event) {
    var focusEle = document.querySelector('.focus');
    if (Navigation.isCurrentPanel('thread')) {
      switch (event.key) {
        case 'Left':
        case 'ArrowLeft':
          if (!ThreadUI.isInEditMode() && !ThreadUI.attachInMessageInput() &&
              ThreadListUI._canSwitchBetweenThreads()) {
            if (Compose.isEmpty()) {
              ThreadListUI.switchPreviousThread();
            } else {
              if (!ThreadUI.confirmDialogShown) {
                ThreadUI.switchThreadSelection(function() {
                  ThreadListUI.switchPreviousThread();
                });
              }
            }
          }
          break;
        case 'Right':
        case 'ArrowRight':
          if (!ThreadUI.isInEditMode() && !ThreadUI.attachInMessageInput() &&
              ThreadListUI._canSwitchBetweenThreads()) {
            if (Compose.isEmpty()) {
              ThreadListUI.switchNextThread();
            } else {
              if (!ThreadUI.confirmDialogShown) {
                ThreadUI.switchThreadSelection(function() {
                  ThreadListUI.switchNextThread();
                });
              }
            }
          }
          break;
        case 'ArrowUp':
        case 'ArrowDown':
          if ((focusEle.tagName === 'A' || focusEle.tagName === 'DIV') &&
              ThreadUI.getParents(focusEle, 'LI').classList
                .contains('message')) {
            event.preventDefault();
            event.stopPropagation();
            ThreadUI.scrollLinkElement(focusEle, event.key);
            ThreadUI.updateSKs();
          }
          break;
        case 'Backspace':
        case 'BrowserBack':
          event.stopPropagation();
          event.preventDefault();
          if (ThreadUI.contactPromptOptionMenuShown) {
            ThreadUI.optionMenu.hide();
            ThreadUI.updateSKs(ThreadUI.currentSoftKey);
            ThreadUI.contactPromptOptionMenuShown = false;
            return;
          }
          if (focusEle.nodeName == 'IFRAME' &&
              focusEle == document.activeElement &&
              focusEle.parentNode == document.querySelector('#messages-input')) {
            Compose.removeAttachment();
            return;
          }
          if (!exports.option.menuVisible) {
            if (ThreadUI.isInEditMode()) {
              ThreadUI.cancelEdit();
              if (Settings.mmsEnable) {
                ThreadUI.updateSKs(threadUiNormalOptions);
              } else {
                ThreadUI.updateSKs(threadUiNormalOptions_noMMS);
              }
              NavigationMap.reset('thread-messages');
              var index = ThreadUI.getItemIndex(NavigationMap.getCurrentControl().elements, 'messages-input');
              NavigationMap.setFocus(index);
            } else {
              if (!ThreadUI.optionMenuShown) {
                ThreadUI.back();
              }
            }
          }
          break;
      }
    } else if (Navigation.isCurrentPanel('composer')) {
      switch (event.key) {
        case 'Backspace':
        case 'BrowserBack':
          event.stopPropagation();
          event.preventDefault();
          if (ThreadUI.confirmDialogShown) {
            // confirm dialog will handle back event
            exports.option.show();
            ThreadUI.confirmDialogShown = false;
            return;
          }
          if (ThreadUI.contactPromptOptionMenuShown) {
            ThreadUI.optionMenu.hide();
            ThreadUI.updateSKs(ThreadUI.currentSoftKey);
            ThreadUI.contactPromptOptionMenuShown = false;
            return;
          }
          if (focusEle.nodeName == 'IFRAME' &&
              focusEle == document.activeElement &&
              focusEle.parentNode == document.querySelector('#messages-input')) {
            Compose.removeAttachment();
          }
          break;

        case 'EndCall':
          ThreadUI.back(true);
          event.preventDefault();
          break;

      }
    }
  },

  // Need pick up the situation that focused on attachment
  attachInMessageInput: function() {
    if ((document.activeElement.parentNode.id === 'messages-input') &&
        (document.activeElement.tagName === 'IFRAME')) {
      return true;
    }

    return false;
  },

  handleInput: function(evt) {
    var currentFocus = document.querySelector('.focus');
    if (currentFocus.nodeName == 'IFRAME') {
      return;
    }
    var range = window.getSelection().getRangeAt(0);
    var currentCursor = range.startContainer;
    switch (evt.key) {
      case 'ArrowUp':
        if (((currentCursor == document.activeElement || currentCursor == document.activeElement.firstChild)
            && range.startOffset == 0)) {
          break;
        }
        evt.stopPropagation();
        break;
      case 'ArrowDown':
        var nextrange = document.activeElement.childNodes.length;
        if (((currentCursor === document.activeElement ||
            currentCursor === document.activeElement.childNodes[nextrange - 2]) && isLastLine(range))) {
          break;
        }
        evt.stopPropagation();
        break;
      case 'ArrowLeft':
        if (!ThreadUI.isInEditMode() && ThreadListUI._canSwitchBetweenThreads()) {
          evt.stopPropagation();
          ThreadListUI.switchPreviousThread();
          break;
        }
        break;
      case 'ArrowRight':
        if (!ThreadUI.isInEditMode() && ThreadListUI._canSwitchBetweenThreads()) {
          evt.stopPropagation();
          ThreadListUI.switchNextThread();
          break;
        }
        break;

      case 'Backspace':
        evt.stopPropagation();
        evt.preventDefault();
        ThreadUI.back();
        break;
    }

    function isLastLine(range) {
      var len = document.activeElement.childNodes.length;
      if (range.startContainer.nodeName === '#text' &&
          range.endContainer === document.activeElement.childNodes[len - 2] &&
          range.startOffset === document.activeElement.childNodes[len - 2].nodeValue.length) {
        return true;
      } else if (range.startContainer.nodeName === 'DIV' &&
          range.startOffset === document.activeElement.childNodes.length - 1) {
        return true;
      }
      return false;
    }
  },

  updateSKs: function(params) {
    var skHidden = false;

    function handleSoftKey(key) {
      if (key !== promptContactOption) {
        ThreadUI.currentSoftKey = key;
      }
      if (exports.option) {
        if (exports.option.initSoftKeyPanel) {
          exports.option.initSoftKeyPanel(key);
        } else {
          exports.option = new SoftkeyPanel(key);
        }
      } else {
        exports.option = new SoftkeyPanel(key);
      }
      if (skHidden) {
        exports.option.hide();
      } else {
        exports.option.show();
      }
    }

    if (document.getElementById('loading').classList.contains('show-loading')) {
      return;
    }

    if (Utils.menuOptionVisible) {
      return;
    }

    if (!params) {
      if (ThreadUI.isInEditMode()) {
        var selected = ThreadUI.selectionHandler.selectedCount;

        if (selected === ThreadUI.allInputs.length) {
          if (ThreadUI._renderingMessage) {
            ThreadUI.updateSKs(selectAllWithDeslectFocusWait);
          } else {
            ThreadUI.updateSKs(selectDeselectAllwithDeleteOptions);
          }
        } else if (selected > 0) {
          if (document.activeElement.querySelectorAll('.thread-checked').length !== 0 ||
              document.activeElement.classList.contains('thread-checked')) {
            if (ThreadUI._renderingMessage) {
              ThreadUI.updateSKs(selectAllWithDeslectFocusWait);
            } else {
              ThreadUI.updateSKs(selectAllWithDeslectFocus);
            }
          } else {
            if (ThreadUI._renderingMessage) {
              ThreadUI.updateSKs(selectAllWithDeleteOptionsWait);
            } else {
              ThreadUI.updateSKs(selectAllWithDeleteOptions);
            }
          }
        } else {
          if (ThreadUI._renderingMessage) {
            ThreadUI.updateSKs(selectAllOptionsWait);
          } else {
            ThreadUI.updateSKs(selectAllOptions);
          }
        }
      }
      if (this.currentMode === 'thread' && !ThreadUI.isInEditMode()) {
        params = {
          header: {l10nId: 'options'},
          items: []
        };
        var focused = document.querySelectorAll('.focus');
        var focusClassList = focused[0].classList;
        if (typeof focused != 'undefined' && focused != null && focused.length > 0) {
          if (focused[0].tagName === 'LI' && focusClassList.contains('message') &&
              !focusClassList.contains('not-downloaded')) {
            if (ThreadUI.getAChildFromMessage(focused[0])) {
              params.items.push(skLinkSelect);
            }
          }
          else if ((focused[0].tagName === 'A' || focused[0].tagName === 'DIV') &&
              this.getParents(focused[0], 'LI').classList.contains('message')) {
            params.items.push(skCancelToThread);
            var linkType = ThreadUI.listenLinkElement(focused[0]);
            switch (linkType) {
              case 'attachment':
                params.items.push(skAttachmentOpen);
                break;
              case 'dial-link':
                params.items.push(skDialOpen);
                params.items.push(skOption);
                break;
              case 'url-link':
                params.items.push(skUrlOpen);
                break;
              case 'email-link':
                if (Settings.emailAppInstalled) {
                  params.items.push(skEmailOpen);
                }
                params.items.push(skOption);
                break;
            }
            handleSoftKey(params);
            return;
          }

          if (!Compose.isEmpty() && (Compose.size <= Settings.mmsSizeLimitation)) {
            params = ThreadUI.selectSIMSend(params);
          }

          if (focusClassList && focusClassList.contains('not-downloaded') &&
              !focusClassList.contains('expired')) {
            params.items.push(skDownload);
            if (focusClassList.contains('pending')) {
              skHidden = true;
            }
          }

          if (focusClassList && focusClassList.contains('error') &&
              focusClassList.contains('outgoing')) {
            params.items.push(skResendThisMessage);
          }

          if (focusClassList && !focusClassList.contains('not-downloaded')) {
            params.items.push(skForward);
          }

          params.items.push(skDelete);
          params.items.push(skViewMessageReport);
        }
      }
    }

    if (ActivityHandler._lastMessage) {
      params = lastMessageOption;
    }

    handleSoftKey(params);
  },

  observeFocusChange_new: function() {
    var suggestionsList = document.querySelector('#messages-recipient-suggestions');
    var suggestionsObserver = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        if (mutation.type == 'attributes') {
          if (Navigation.isCurrentPanel('composer') && !suggestionsList.classList.contains('hide')) {
            if (suggestionsList.firstElementChild.children.length > 0 && suggestionsList.firstElementChild.querySelector('.selected')) {
              ThreadUI.updateSKs(threadUiAddSuggestion);
            } else {
              ThreadUI.updateSKs(threadUIAddContactWithInput);
            }
          } else if (Navigation.isCurrentPanel('composer')
              && suggestionsList.classList.contains('hide')) {
            var currentFocus = document.querySelector('.focus');
            if (currentFocus && currentFocus.tagName != 'DIV') {
              ThreadUI.updateSKs(threadUIAddContactWithInput);
            }
          }
        }
      });
    });
    var suggestionsConfig = {
      attributes: true,
      subtree: true,
      attributeOldValue: true
    };
    suggestionsObserver.observe(suggestionsList, suggestionsConfig);

    var input = document.getElementById('messages-input');
    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        if (mutation.type == 'attributes') {
          if (mutation.attributeName == 'class') {
            ThreadUI.onFocusChanged();
          }
        } else if (mutation.type == 'childList') {
          if ((mutation.addedNodes.length > 0 && mutation.addedNodes[0].tagName == 'IFRAME')
              || (mutation.removedNodes.length > 0 && mutation.removedNodes[0].tagName == 'IFRAME' )) {
            NavigationMap.reset('thread-messages');     //reset navigation when attachment added
          }
          if (mutation.removedNodes.length > 0 && mutation.removedNodes[0].tagName == 'IFRAME') {
            var focused = document.querySelector('.focus');
            if (!focused) {
              input.classList.add('focus');
              input.focus();
            }
          }
        }
      });
    });
    var config = {
      attributes: true,
      childList: true
    };
    observer.observe(input, config);
  },

  getParents: function(node, tagName) {
    var parent = node.parentNode;
    var tag = tagName.toUpperCase();
    if (parent.tagName === tag) {
      return parent;
    } else if (parent.tagName === 'BODY') {
      return document.body;
    } else {
      return this.getParents(parent, tag);
    }
  },

  getAChildFromMessage: function(node) {
    var secondFocusElement = node
      .getElementsByClassName('message-content-body')[0];
    if (secondFocusElement.querySelectorAll('.link-focus').length === 0) {
      return false;
    } else {
      return true;
    }
  },

  findLinkFocus: function () {
    var parentNode = document.activeElement;
    this.scrollIndex = 0;
    var secondNode = parentNode.getElementsByClassName('message-content-body')[0]
      .querySelectorAll('.link-focus');
    parentNode.blur();
    parentNode.classList.remove('focus');
    secondNode[this.scrollIndex].focus();
    secondNode[this.scrollIndex].classList.add('focus');
    ThreadUI.updateSKs();
  },

  openLinkFocus: function (type) {
    var focusEle = document.querySelector('.focus');
    switch (type) {
      case 'dial':
        ThreadUI.promptContact({
          number: focusEle.dataset.dial,
          inMessage: true
        });
        break;
      case 'email':
        ThreadUI.promptContact({
          email: focusEle.dataset.email,
          inMessage: true
        });
        break;
    }
  },

  doLinkAction: function (type) {
    function dialCallBack() {
      ThreadUI.updateSKs({
        header: {l10nId: 'options'},
        items: [skCancelToThread, skDialOpen, skOption]
      });
    }

    var focusEle = document.querySelector('.focus');
    switch (type) {
      case 'attachment':
        thui_mmsAttachmentClick(focusEle);
        break;
      case 'dial':
        ActivityPicker.dial(focusEle.dataset.dial,
          true, dialCallBack, dialCallBack);
        break;
      case 'email':
        ActivityPicker.email(focusEle.dataset.email,
          function() {
            ThreadUI.updateSKs(ThreadUI.currentSoftKey);
          }, function() {
            ThreadUI.updateSKs(ThreadUI.currentSoftKey);
          });
        break;
      case 'url':
        ActivityPicker[type](focusEle.dataset[type]);
        break;
    }
  },

  scrollLinkElement: function (node, direction) {
    var sourceElement = ThreadUI.getParents(node, 'LI');
    var secondNode = sourceElement
      .getElementsByClassName('message-content-body')[0];
    var secondElemnet = secondNode.querySelectorAll('.link-focus');
    var elementLength = secondElemnet.length;
    var nextFocusNode;
    if (direction === 'ArrowDown') {
      this.scrollIndex += 1;
    } else {
      this.scrollIndex -= 1;
    }

    if (this.scrollIndex < 0) {
      this.scrollIndex = elementLength - 1;
    }

    if (this.scrollIndex > elementLength - 1) {
      this.scrollIndex = 0;
    }

    nextFocusNode = secondElemnet[this.scrollIndex];
    if (nextFocusNode.tagName === 'A' || nextFocusNode.tagName === 'DIV') {
      node.blur();
      node.classList.remove('focus');
      secondElemnet[this.scrollIndex].focus();
      secondElemnet[this.scrollIndex].classList.add('focus');
      secondElemnet[this.scrollIndex].scrollIntoView(true);
    }
  },

  listenLinkElement: function (node) {
    var isAttachment = attachmentMap.get(node);
    if (isAttachment) {
      return 'attachment';
    } else {
      var linkType = node.getAttribute('data-action');
      return linkType;
    }
  },

  cancelToThread: function () {
    var currentFocus = document.querySelector('.focus');
    var nextFocus = this.getParents(currentFocus, 'LI');
    currentFocus.blur();
    currentFocus.classList.remove('focus');
    nextFocus.focus();
    nextFocus.classList.add('focus');
    this.scrollIndex = 0;
    this.updateSKs();
  },

  selectMessages: function() {
    function startEditMode() {
      if (!window.option || !window.option.menuVisible) {
        document.removeEventListener('transitionend', startEditMode);
      }
      var items = document.querySelectorAll('#messages-container li.navigable');
      NavigationMap.setFocus(items.length - 1);
    }

    function updateSoftkey() {
      window.removeEventListener('messages-render-complete', updateSoftkey);
      ThreadUI.updateSKs();
    }

    ThreadUI.startEdit();
    NavigationMap.reset('messages-container');
    if (ThreadUI._renderingMessage) {
      window.addEventListener('messages-render-complete', updateSoftkey);
    }

    document.addEventListener('transitionend', startEditMode);
  },

  showToaster: function(mode, count) {
    var param = {
      latency: 2000
    };
    if (mode === 'messageMode') {
      param.messageL10nId = 'delete-message';
    }
    else if (mode === 'threadMode') {
      param.messageL10nId = 'deleted-threads';
    }
    else if (mode === 'editMode') {
      param.messageL10nId = 'deleted-message';
      param.messageL10nArgs = {n: count};
    }
    Toaster.showToast(param);
  },

  deleteCurrentThread: function(mode) {
    var messageIdsToDelete = [];
    var threadId = ThreadUI.currentThreadId;

    function onAllThreadMessagesRetrieved() {
      MessageManager.deleteMessages(messageIdsToDelete);
      ThreadListUI.deleteThread(threadId);
      WaitingScreen.hide();
      Compose.clear();
      ThreadUI.back();
      messageIdsToDelete = null;
    }

    function onThreadMessageRetrieved(message) {
      messageIdsToDelete.push(message.id);
      return true;
    }

    function performDelete() {
      WaitingScreen.show();
      if (exports.option) {
        exports.option.hide();
      }
      MessageManager.getMessages({
        filter: { threadId:  threadId},
        each: onThreadMessageRetrieved,
        end: onAllThreadMessagesRetrieved
      });
    }

    function confirmCallback() {
      performDelete();
    }

    if (threadId) {
      if (mode === 'messageMode') {
        var confirmation = 'deleteMessage-confirmation';
      }
      else if (mode === 'editMode') {
        var confirmation = 'deleteMessages-confirmation';
      } else {
        var confirmation = 'deleteMessage-confirmation2';
      }
      Utils.confirmAlert('confirmation-title', confirmation,
                         'cancel', null, null, null,
                         'delete', confirmCallback);
    }
    Utils.menuOptionVisible = false;
  },

  deleteSingleMessage: function() {
    var messageId = ThreadUI._getFocusedMessageId(ThreadUI._storeFocused);
    Utils.confirmAlert('confirmation-title', 'deleteMessage-confirmation',
                       'cancel', null, null, null, 'delete', confirmCallback);
    Utils.menuOptionVisible = false;
    function confirmCallback() {
      WaitingScreen.show();
      if (exports.option) {
        exports.option.hide();
      }
      MessageManager.deleteMessages(
        messageId, () => ThreadUI.deleteUIMessages(messageId)
      );
      ThreadUI.showToaster('messageMode');
      NavigationMap.reset('thread-messages');
      var index = ThreadUI.getItemIndex(
        NavigationMap.getCurrentControl().elements, 'messages-input');
      NavigationMap.setFocus(index);
      WaitingScreen.hide();
    }
  },

  simSelectCall: function thui_simSelectCall(callback) {
    // Bug633-gang-chen@t2mobile.com-DSDS no name-begin
    var conns = window.navigator.mozMobileConnections;
    var items = [];
    var params = {
      classes: ['group-menu', 'softkey'],
      header: navigator.mozL10n.get('select') || '',
      items: null,
      menuClassName: 'menu-button'
    };

    if (conns[0] && conns[0].voice.network) {
        var carrier0 = conns[0].voice.network.shortName
            || conns[0].voice.network.longName;
    }
    items.push({
      name: carrier0 ? 'SIM1 - ' + carrier0 : 'SIM1',
      method: function() {
        if (conns[0]) {
          callback(0);
        }
      },
      disable: conns[0] ? false : true
    });

    if (conns[1] && conns[1].voice.network) {
        var carrier1 = conns[1].voice.network.shortName
            || conns[1].voice.network.longName;
    }
    items.push({
      name: carrier1 ? 'SIM2 - ' + carrier1 : 'SIM2',
      method: function() {
        if (conns[1]) {
          callback(1);
        }
      },
      disable: conns[1] ? false : true
    });
    // Bug633-gang-chen@t2mobile.com-DSDS no name-end

    params.items = items;
    this.optionMenu = new OptionMenu(params);
    this.optionMenu.show();
    ThreadUI.contactPromptOptionMenuShown = true;
    ThreadUI.updateSKs(promptContactOption);
  },

  simSelectOptions: function thui_simSelect(callback) {
    var conns = window.navigator.mozMobileConnections;
    var items = [];
    var params = {
      classes: ['group-menu', 'softkey'],
      header: navigator.mozL10n.get('select') || '',
      items: null,
      menuClassName: 'menu-button'
    };

    if (conns[0] && conns[0].voice.network) {
      var carrier0 = conns[0].voice.network.shortName
                  || conns[0].voice.network.longName;
    }
    // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name begin
    var nameOne = Settings.sim1Name ? Settings.sim1Name : 'SIM1';
    items.push({
      name: carrier0 ? nameOne + ' - ' + carrier0 : nameOne,
      method: function() {
        if (conns[0]) {
          if (callback) {
            callback(0);
          } else {
            ThreadUI.simSelectedCallback(0);
          }
        }
      },
      disable: conns[0] ? false : true
    });

    if (conns[1] && conns[1].voice.network) {
      var carrier1 = conns[1].voice.network.shortName
                  || conns[1].voice.network.longName;
    }
    var nameTwo = Settings.sim2Name ? Settings.sim2Name : 'SIM2';
    items.push({
      name: carrier1 ? nameTwo + ' - ' + carrier1 : nameTwo,
      method: function() {
        if (conns[1]) {
          if (callback) {
            callback(1);
          } else {
            ThreadUI.simSelectedCallback(1);
          }
        }
      },
      disable: conns[1] ? false : true
    });
    // Bug876-gang-chen@t2mobile.com-FR DSDS sim card name end
    params.items = items;

    this.optionMenu = new OptionMenu(params);
    this.optionMenu.show();
    ThreadUI.contactPromptOptionMenuShown = true;
    setTimeout(() => {
      ThreadUI.updateSKs(promptContactOption);
    }, 200);
  },

  // FIXME/bug 983411: phoneNumber not needed.
  simSelectedCallback: function thui_simSelected(cardIndex) {
    if (Compose.isEmpty()) {
      return;
    }

    cardIndex = +cardIndex;
    if (isNaN(cardIndex)) {
      cardIndex = 0;
    }

    this.sendMessage({ serviceId: cardIndex });
  },

  sendMessage: function thui_sendMessage(opts) {
    // bug3617-chengyanzhang@t2mobile.com-for Argon doesn't support sms over wifi-begin
    /*if (!this.isWifiCallingAvaiable(opts.serviceId)) {
      return;
    }*/
    // bug3617-chengyanzhang@t2mobile.com--for Argon doesn't support sms over wifi-end

    var content = Compose.getContent(),
        subject = Compose.getSubject(),
        messageType = Compose.type,
        serviceId = opts.serviceId === undefined ? null : opts.serviceId,
        recipients;

    var inComposer = Navigation.isCurrentPanel('composer');

    // Depending where we are, we get different nums
    if (inComposer) {
      if (!this.recipients.length) {
        console.error('The user tried to send the message but no recipients ' +
            'are entered. This should not happen because we should disable ' +
            'the send button in that case');
        return;
      }
      recipients = this.recipients.numbers;
    } else {
      recipients = Threads.active.participants;
    }

    // Clean composer fields (this lock any repeated click in 'send' button)
    this.disableConvertNoticeBanners();
    this.cleanFields();
    this.enableConvertNoticeBanners();

    // If there was a draft, it just got sent
    // so delete it
    if (this.draft) {
      ThreadListUI.removeThread(this.draft.id);
      Drafts.delete(this.draft).store();
      this.draft = null;
    }

    this.updateHeaderData();

    this.shouldChangePanelNextEvent = inComposer;

    // Send the Message
    if (messageType === 'sms') {
      MessageManager.sendSMS({
        recipients: recipients,
        content: content[0],
        serviceId: serviceId,
        oncomplete: function onComplete(requestResult) {
          if (!requestResult.hasError) {
            this.onMessageSendRequestCompleted();
            return;
          }

          var errors = {};
          requestResult.return.forEach(function(result) {
            if (result.success) {
              return;
            }

            if (errors[result.code.name] === undefined) {
              errors[result.code.name] = [result.recipient];
            } else {
              errors[result.code.name].push(result.recipient);
            }
          });

          for (var key in errors) {
            this.showMessageSendingError(key, {recipients: errors[key]});
          }
        }.bind(this)
      });

      if (recipients.length > 1) {
        this.shouldChangePanelNextEvent = false;
        Navigation.toPanel('thread-list');
        ThreadListUI.updateThread(Threads.active);
        if (ActivityHandler.isInActivity()) {
          setTimeout(this.close.bind(this), this.LEAVE_ACTIVITY_DELAY);
        }

        // Early return to prevent compose focused for multi-recipients sms case
        return;
      }

    } else {
      if (Settings.dataServiceId === -1) {
        this.showMessageSendingError('NoSignalError');
        return;
      }

      if ((Settings.dataServiceId !== serviceId) &&
          (Navigation.isCurrentPanel('composer'))) {
        ThreadUI.switchFlag = true;
      }

      var smilSlides = content.reduce(thui_generateSmilSlides, []);
      var mmsOpts = {
        recipients: recipients,
        subject: subject,
        content: smilSlides,
        serviceId: serviceId,
        onsuccess: function() {
          this.onMessageSendRequestCompleted();
        }.bind(this),
        onerror: function onError(error) {
          var errorName = error.name;
          this.showMessageSendingError(errorName);
        }.bind(this)
      };

      MessageManager.sendMMS(mmsOpts);
    }
  },

  onMessageSent: function thui_onMessageSent(e) {
    this.setMessageStatus(e.message.id, 'sent');
    this.scrollViewToBottom();
    if (Startup.isActivity) {
      ActivityHandler.leaveActivity();
    }
    Toaster.showToast({
      messageL10nId: 'message-sent-toast',
      latency: 2000
    });
  },

  /**
   * Fires once message (both SMS and MMS) send/resend request initiated by the
   * current application instance is successfully completed.
   */
  onMessageSendRequestCompleted: function thui_onMessageSendRequestCompleted() {
    // Play the audio notification
    if (this.sentAudioEnabled) {
      this.sentAudio.play();
    }
  },

  onMessageFailed: function thui_onMessageFailed(e) {
    function alertComfirm() {
      ThreadUI.confirmDialogShown = false;
    }

    var message = e.message;
    if (Startup.isActivity) {
      Toaster.showToast({
        messageL10nId: 'send-failed',
        latency: 2000
      });
      ActivityHandler.leaveActivity();
      return;
    }

    // When this is the first message in a thread, we haven't displayed
    // the new thread yet. The error flag will be shown when the thread
    // will be rendered. See Bug 874043

    if (this.showErrorInFailedEvent === 'NonActiveSimCardError') {
      this.showErrorInFailedEvent = '';
      // Because the dataServiceId are 0 or 1, use total and
      // current serviceId to get the another useless sim serviceId.
      var anotherSIMId = 1 - Settings.dataServiceId;
      // The serviceId need add 1 to get the true SIM card.
      var anotherSIMCard = anotherSIMId + 1;
      Utils.confirmAlert('switch-SIM-data-send-alert-title',
        {
          id: 'switch-SIM-data-send-alert-body',
          args: { n: anotherSIMCard }
        },
        null, null, 'ok', alertComfirm, null, null, alertComfirm);
      this.confirmDialogShown = true;
    }

    // The all error logic are confirmed at gecko to make the gaia simple,
    // but the fail message may be received before sending message building
    // sometimes, so we need listen a event to mark error flag.
    function messageDOMCreated() {
      window.removeEventListener('message-dom-created', messageDOMCreated);
      ThreadUI.setMessageStatus(message.id, 'error');
      ThreadUI.scrollViewToBottom();
    }

    var messageDOM = document.getElementById('message-' + message.id);
    if (messageDOM) {
      this.setMessageStatus(message.id, 'error');
      this.scrollViewToBottom();
    } else {
      window.addEventListener('message-dom-created', messageDOMCreated);
    }
  },

  onDeliverySuccess: function thui_onDeliverySuccess(e) {
    var message = e.message;
    // We need to make sure all the recipients status got success event.
    if (!this.shouldShowDeliveryStatus(message)) {
      return;
    }

    this.setMessageStatus(message.id, 'delivered');
  },

  onReadSuccess: function thui_onReadSuccess(e) {
    var message = e.message;
    // We need to make sure all the recipients status got success event.
    if (!this.shouldShowReadStatus(message)) {
      return;
    }

    this.setMessageStatus(message.id, 'read');
  },

  onMessageRetrieving: function thui_onMessageRetrieving(e) {
    Toaster.showToast({
      messageL10nId: 'downloading-attachment',
      latency: 3000
    });
  },

  // Some error return from sending error need some specific action instead of
  // showing the error prompt directly.
  showMessageSendingError: function thui_showMsgSendingError(errorName, opts) {
    // TODO: We handle NonActiveSimCard error in onMessageFailed because we
    // could not get message id from this error handler. Need to be removed when
    // bug 824717 is landed.
    if (errorName === 'NonActiveSimCardError') {
      this.showErrorInFailedEvent = errorName;
      return;
    }
    if (errorName === 'NotFoundError') {
      console.info('The message was deleted or is no longer available.');
      return;
    }
    this.confirmDialogShown = true;
    this.showMessageError(errorName, opts);
  },

  showMessageError: function thui_showMessageOnError(errorCode, opts) {
    var dialog = new ErrorDialog(Errors.get(errorCode), opts);
    dialog.show();
  },

  removeMessageDOM: function thui_removeMessageDOM(messageDOM) {
    var messagesContainer = messageDOM.parentNode;

    messageDOM.remove();

    // If we don't have any other messages in the list, then we remove entire
    // date group (date header + messages container).
    if (messagesContainer &&!messagesContainer.firstElementChild) {
      messagesContainer.parentNode.remove();
    }
  },

  setMessageStatus: function(id, status) {
    var messageDOM = document.getElementById('message-' + id);

    if (!messageDOM || messageDOM.classList.contains(status)) {
      return;
    }

    var newStatusMarkup = this.getMessageStatusMarkup(status),
        oldStatusNode = messageDOM.querySelector('.message-status');

    messageDOM.classList.remove(
      'sending', 'pending', 'sent', 'received', 'delivered', 'read', 'error'
    );
    messageDOM.classList.add(status);
    messageDOM.querySelector('.deliver-mark').classList
      .toggle('hide', (status === 'delivered' ? false : true));

    if (oldStatusNode) {
      oldStatusNode.remove();
    }

    if (newStatusMarkup) {
      messageDOM.querySelector('.message-content-body-container').appendChild(
        newStatusMarkup.toDocumentFragment()
      );
    }
  },

  retrieveMMS: function thui_retrieveMMS(messageDOM) {
    // force a number
    var id = +messageDOM.dataset.messageId;
    var iccId = messageDOM.dataset.iccId;

    var request = MessageManager.retrieveMMS(id);

    var button = messageDOM.querySelector('button');

    this.setMessageStatus(id, 'pending');
    button.setAttribute('data-l10n-id', 'downloading-attachment');
    exports.option.hide();

    this.updateSKs();

    request.onsuccess = (function retrieveMMSSuccess() {
      this.removeMessageDOM(messageDOM);
      Toaster.showToast({
        messageL10nId: 'download-complete',
        latency: 3000
      });
    }).bind(this);

    request.onerror = (function retrieveMMSError() {
      this.setMessageStatus(id, 'error');
      button.setAttribute('data-l10n-id', 'download-attachment');

      // Show NonActiveSimCard/Other error dialog while retrieving MMS
      var errorCode = (request.error && request.error.name) ?
        request.error.name : null;

      if (!navigator.mozSettings) {
        console.error('Settings unavailable');
        return;
      }

      // Replacing error code to show more specific error message for this case
      if (errorCode === 'RadioDisabledError') {
        errorCode = 'RadioDisabledToDownloadError';
      }

      var serviceId = Settings.getServiceIdByIccId(iccId);
      if (errorCode === 'NonActiveSimCardError') {
        // Because the dataServiceId are 0 or 1, use total and
        // current serviceId to get the another useless sim serviceId.
        var anotherSIMId = 1 - Settings.dataServiceId;
        // The serviceId need add 1 to get the true SIM card.
        var anotherSIMCard = anotherSIMId + 1;
        Utils.confirmAlert('switch-SIM-data-receive-alert-title',
          {
            id: 'switch-SIM-data-receive-alert-body',
            args: { n: anotherSIMCard }
          },
          null, null, 'ok', null, null, null);
        return;
      }

      if (errorCode) {
        this.showMessageError(errorCode, {
          confirmHandler: function stateResetAndRetry() {
            if (serviceId === null) {
              // TODO Bug 981077 should change this error message
              this.showMessageError('NoSimCardError');
            }
          }.bind(this)
        });
      }
    }).bind(this);
  },

  resendMessage: function thui_resendMessage(id) {
    // force id to be a number
    id = +id;

    var request = MessageManager.getMessage(id);

    request.onsuccess = (function() {
      var message = request.result;
      var serviceId = Settings.getServiceIdByIccId(message.iccId);
      serviceId = serviceId === null ? 0 : serviceId;
      // bug3617-chengyanzhang@t2mobile.com-for Argon doesn't support sms over wifi-begin
      /*if (!this.isWifiCallingAvaiable(serviceId)) {
        return;
      }*/
      // bug3617-chengyanzhang@t2mobile.com-for Argon doesn't support sms over wifi-end
      // Strategy:
      // - Delete from the DOM
      // - Resend (the resend will remove from the backend)
      // - resend accepts a optional callback that follows with
      // the result of the resending
      var messageDOM = document.getElementById('message-' + id);
      var resendOpts = {
        message: message,
        onerror: function onError(error) {
          var errorName = error.name;
          this.showMessageSendingError(errorName);
        }.bind(this),
        onsuccess: function() {
           this.onMessageSendRequestCompleted();
        }.bind(this)
      };
      this.removeMessageDOM(messageDOM);
      MessageManager.resendMessage(resendOpts);
    }).bind(this);
  },

  toFieldKeypress: function(event) {
    if (event.keyCode === 13 || event.keyCode === event.DOM_VK_ENTER) {
      this.toggleRecipientSuggestions();
    }
  },

  toFieldInput: function(event) {
    var typed;

    this.toField.classList.toggle('recipient-multi-line',
                                  this.toField.offsetHeight > this.RECIPIENTS_INPUT_FIELD_MAX_ONE);

    if (event.target.isPlaceholder) {
      typed = event.target.textContent.trim();
      if (!typed) {
        this.stopContactNavigation();
      }
      this.searchContact(typed, this.listContacts.bind(this));
    }

    this.emit('recipientschange');
  },

  exactContact: function thui_searchContact(fValue, handler) {
    Contacts.findExact(fValue, handler.bind(null, fValue));
  },

  searchContact: function thui_searchContact(fValue, handler) {
    if (!fValue) {
      // In cases where searchContact was invoked for "input"
      // that was actually a "delete" that removed the last
      // character in the recipient input field,
      // eg. type "a", then delete it.
      // Always remove the the existing results.
      this.toggleRecipientSuggestions();
      return;
    }

    Contacts.findByString(fValue, handler.bind(null, fValue));
  },

  validateContact: function thui_validateContact(source, fValue, contacts) {
    var isInvalid = true;
    var index = this.recipients.length - 1;
    var last = this.recipientsList.lastElementChild;
    var typed = last && last.textContent.trim();
    var isContact = false;
    var record, length, number, contact;

    if (index < 0) {
      index = 0;
    }

    var props = ['tel'];
    if (Settings.supportEmailRecipient) {
      props.push('email');
    }

    // If there is greater than zero matches,
    // process the first found contact into
    // an accepted Recipient.
    if (contacts && contacts.length) {
      isInvalid = false;
      record = contacts[0];
      var values = props.reduce((values, prop) => {
        var propValue = record[prop];
        if (propValue && propValue.length) {
          return values.concat(propValue);
        }

        return values;
      }, []);
      length = values.length;

      // Received an exact match with a single tel or email record
      if (source.isLookupable && !source.isQuestionable && length === 1) {
        if (Utils.probablyMatches(values[0].value, fValue)) {
          isContact = true;
          number = values[0].value;
        }
      } else {
        // Received an exact match that may have multiple tel records
        for (var i = 0; i < length; i++) {
          var propValue = values[i].value;
          if (this.recipients.numbers.indexOf(propValue) === -1) {
            number = propValue;
            break;
          }
        }

        // If number is not undefined, then it's safe to assume
        // that this number is unique to the recipient list and
        // can be added as an accepted recipient from the user's
        // known contacts.
        //
        // It _IS_ possible for this to appear to be a duplicate
        // of an existing accepted recipient: by display name ONLY;
        // however this case will always have a different number.
        //
        if (typeof number !== 'undefined') {
          isContact = true;
        } else {
          // If no number match could be made, then this
          // contact record is actually inValid.
          isInvalid = true;
        }
      }
    }

    // Either an exact contact with a single tel record was matched
    // or an exact contact with multiple tel records and we've taken
    // one of the non-accepted tel records to add a new recipient.
    if (isContact) {

      // Remove the last assimilated recipient entry.
      this.recipients.remove(index);

      contact = Utils.basicContact(number, record);
      contact.source = 'contacts';

      // Add the newly minted contact as an accepted recipient
      this.recipients.add(contact).focus();

      return;
    }

    // Received multiple contact matches and the current
    // contact record had a number that has already been
    // accepted as a recipient. Try the next contact in the
    // set of results.
    if (isInvalid && contacts.length > 1) {
      this.validateContact(source, fValue, contacts.slice(1));
      return;
    }

    // Plain numbers with no contact matches can never be "invalid"
    if (!source.isQuestionable && !length) {
      isInvalid = false;
    }

    // If there are no contacts matched
    // this input was definitely invalid.
    source.isInvalid = isInvalid;

    // Avoid colliding with an "edit-in-progress".
    if (!typed) {
      this.recipients.update(index, source).focus();
    }
  },

  listContacts: function thui_listContacts(fValue, contacts) {
    // If the user has cleared the typed input before the
    // results came back, prevent the results from being rendered
    // by returning immediately.
    if (!this.recipients.inputValue) {
      return;
    }

    this.toggleRecipientSuggestions();
    if (!contacts || !contacts.length) {
      if (this.listener) {
        this.stopContactNavigation();
      }
      return;
    }

    // There are contacts that match the input.
    var suggestionList = document.createDocumentFragment();

    // Render each contact in the contacts results
    var renderer = ContactRenderer.flavor('suggestion');
    var unknownContactsRenderer = ContactRenderer.flavor('suggestionUnknown');

    contacts.forEach(function(contact) {
      var rendererArg = {
        contact: contact,
        input: fValue,
        target: suggestionList,
        skip: this.recipients.numbers
      };
      if (contact.source != 'unknown') {
        renderer.render(rendererArg);
      }
      else {
        unknownContactsRenderer.render(rendererArg);
      }
    }, this);

    if (suggestionList.children.length > 0) {
      if (!this.listener) {
        this.startContactNavigation();
      }
      this.toggleRecipientSuggestions(suggestionList);
    } else if (this.listener) {
      this.stopContactNavigation();
    }
  },

  onHeaderActivation: function thui_onHeaderActivation() {
    // Do nothing while in participants list view.
    if (!Navigation.isCurrentPanel('thread')) {
      return;
    }

    var participants = Threads.active && Threads.active.participants;

    // >1 Participants will enter "group view"
    if (participants && participants.length > 1) {
      Navigation.toPanel('group-view', {
        id: Threads.currentId
      });
      return;
    }

    var number = this.headerText.dataset.number;

    var tel, email;
    if (Settings.supportEmailRecipient && Utils.isEmailAddress(number)) {
      email = number;
    } else {
      tel = number;
    }

    if (this.headerText.dataset.isContact === 'true') {
      this.promptContact({
        number: number
      });
    } else {
      this.prompt({
        number: tel,
        email: email,
        isContact: false
      });
    }
  },

  promptContact: function thui_promptContact(opts) {
    opts = opts || {};

    var inMessage = opts.inMessage || false;
    var number = opts.number || opts.email || '';
    var tel, email;

    if (Settings.supportEmailRecipient && Utils.isEmailAddress(number)) {
      email = number || '';
    } else {
      tel = number || '';
    }

    Contacts.findByAddress(number, function(results) {
      var isContact = results && results.length;
      var contact = results[0];
      var id;

      var fragment;

      if (isContact) {
        id = contact.id;

        fragment = document.createDocumentFragment();

        ContactRenderer.flavor('prompt').render({
          contact: contact,
          input: number,
          target: fragment
        });
      }

      this.prompt({
        number: tel,
        email: email,
        header: fragment,
        contactId: id,
        contact: contact,
        isContact: isContact,
        inMessage: inMessage
      });
    }.bind(this));
  },

  prompt: function thui_prompt(opt) {
    // If there is option menu, not create it again.
    if (document.getElementById('option-menu')) {
      return;
    }
    
    var complete = (function complete() {
      if (!Navigation.isCurrentPanel('thread')) {
        Navigation.toPanel('thread', { id: Threads.currentId });
      }
    }).bind(this);

    var thread = Threads.get(Threads.currentId);
    var number = opt.number || '';
    var email = opt.email || '';
    var isContact = opt.isContact || false;
    var inMessage = opt.inMessage || false;
    var header = opt.header;
    var contact = opt.contact;
    var items = [];
    var params, props;

    // Create a params object.
    //  - complete: callback to be invoked when a
    //      button in the menu is pressed
    //  - header: string or node to display in the
    //      in the header of the option menu
    //  - items: array of options to display in menu
    //
    if (!header && (number || email)) {
      header = document.createElement('bdi');
      header.className = 'unknown-contact-header';
      header.textContent = number || email;
    }

    params = {
      classes: ['group-menu', 'softkey'],
      complete: complete,
      header: header || '',
      items: null,
      menuClassName: 'menu-button'
    };

    // All non-email activations (except for single participant thread) will
    // see a "Call" option
    if (!email) {
      // Multi-participant activations or in-message numbers
      // will include a "Call" and "Send Message" options in the menu
      if ((thread && thread.participants.length > 1) || inMessage) {
        items.push({
          l10nId: 'sendMessage',
          method: function oMessage(number, contact) {
            ActivityPicker.sendMessage(number, contact);
            ThreadUI.contactPromptOptionMenuShown = false;
          },
          params: [number, contact],
          // As activity picker changes the panel we don't want
          // to call 'complete' that changes the panel as well
          incomplete: true
        });
      }
    }

    params.items = items;

    if (!isContact) {

      props = [
        number ? {tel: number} : {email: email}
      ];

      // Unknown participants will have options to
      //  - Create A New Contact
      //  - Add To An Existing Contact
      //
      params.items.push({
          l10nId: 'createNewContact',
          method: function oCreate(param) {
            ActivityPicker.createNewContact(
              param, ThreadUI.onCreateContact, function() {
                ThreadUI.updateSKs(ThreadUI.currentSoftKey);
                window.focus();
              }
            );
            ThreadUI.contactPromptOptionMenuShown = false;
          },
          params: props
        },
        {
          l10nId: 'addToExistingContact',
          method: function oAdd(param) {
            ActivityPicker.addToExistingContact(
              param, ThreadUI.onCreateContact, function() {
                ThreadUI.updateSKs(ThreadUI.currentSoftKey);
                window.focus();
              }
            );
            ThreadUI.contactPromptOptionMenuShown = false;
          },
          params: props
        }
      );
    }

    if (opt.contactId && !ActivityHandler.isInActivity()) {

        props = [{ id: opt.contactId }];

        params.items.push({
          l10nId: 'viewContact',
          method: function oView(param) {
            ActivityPicker.viewContact(
              param, function() {
                ThreadUI.updateSKs(ThreadUI.currentSoftKey);
              }, function() {
                ThreadUI.updateSKs(ThreadUI.currentSoftKey);
                window.focus();
              }
            );
            ThreadUI.contactPromptOptionMenuShown = false;
          },
          params: props
        }
      );
    }

    // Menu should not be displayed if no option required, otherwise all
    // activations will see a "Cancel" option
    if (params.items.length === 0) {
      return;
    }

    if (!ThreadUI.optionMenuShown) {
      if (typeof OptionMenu == 'undefined') {
        LazyLoader.load(['/shared/js/option_menu.js',
                       '/style/action_menu.css',
                       '/shared/style/option_menu.css'], () => {
        this.optionMenu = new OptionMenu(params);
        this.optionMenu.show();
        ThreadUI.contactPromptOptionMenuShown = true;
        ThreadUI.updateSKs(promptContactOption);
        });
      } else {
        this.optionMenu = new OptionMenu(params);
        this.optionMenu.show();
        ThreadUI.contactPromptOptionMenuShown = true;
        ThreadUI.updateSKs(promptContactOption);
      }
    }
  },

  onCreateContact: function thui_onCreateContact() {
    ThreadListUI.updateContactsInfo();
    // Update Header if needed
    if (Navigation.isCurrentPanel('thread')) {
      ThreadUI.updateHeaderData();
      ThreadUI.updateSKs(ThreadUI.currentSoftKey);
    }
  },

  discardDraft: function thui_discardDraft() {
    // If we were tracking a draft
    // properly update the Drafts object
    // and ThreadList entries
    if (this.draft) {
      Drafts.delete(this.draft).store();
      if (Threads.active) {
        ThreadListUI.updateThread(Threads.active);
      } else {
        ThreadListUI.removeThread(this.draft.id);
      }
      this.draft = null;
    }
    if ((!Compose.isEmpty() || (ThreadUI.recipients && ThreadUI.recipients.length))
        && !this.EndKeySave) {
      ThreadListUI.onDraftDiscarded();
    }
  },

   /**
   * saveDraft
   *
   * Saves the currently unsent message content or recipients
   * into a Draft object.  Preserves the currently marked
   * draft if specified.  Draft preservation is intended to
   * keep this.draft populated with the currently
   * showing draft when the app is hidden, so when the app
   * comes out of hiding, it knows there is a draft to continue
   * to keep track of.
   *
   * @param {Object} opts Optional parameters for saving a draft.
   *                  - preserve, boolean whether or not to preserve draft.
   *                  - autoSave, boolean whether this is an auto save.
   */
  saveDraft: function thui_saveDraft(opts) {
    var content, draft, recipients, subject, thread, threadId, type;

    content = Compose.getContent();
    subject = Compose.getSubject();
    type = Compose.type;

    if (Threads.active) {
      recipients = Threads.active.participants;
      threadId = Threads.currentId;
    } else {
      recipients = this.recipients.numbers;
    }

    var draftId = this.draft ? this.draft.id : null;

    MessageManager.findThreadFromNumber(recipients).then(
      function onResolve(Id) {
        return new Promise(function(resolve) {
          resolve(Id);
        });
      },
      function onReject() {
        return new Promise(function(resolve) {
          resolve(null);
        });
      }
    ).then(function(messageThreadId) {
      if (!threadId) {
        threadId = messageThreadId;
      }

      draft = new Draft({
        recipients: recipients,
        content: content,
        subject: subject,
        threadId: threadId,
        type: type,
        id: draftId
      });

      Drafts.add(draft);

      // If an existing thread list item is associated with
      // the presently saved draft, update the displayed Thread
      if (threadId) {
        thread = Threads.get(threadId) || Threads.active;

        // Overwrite the thread's own timestamp with
        // the drafts timestamp.

        ThreadListUI.updateThread(thread, {timestamp: draft.timestamp});
      } else {
        ThreadListUI.updateThread(draft);
      }

      // Clear the MessageManager draft if
      // not explicitly preserved for the
      // draft replacement case
      if (!opts || (opts && !opts.preserve)) {
        ThreadUI.draft = null;
      }

      // Set the MessageManager draft if it is
      // not already set and meant to be preserved
      if (!ThreadUI.draft && (opts && opts.preserve)) {
        ThreadUI.draft = draft;
      }

      // Show draft saved banner if not an
      // auto save operation
      if (!opts || (opts && !opts.autoSave)) {
        ThreadListUI.onDraftSaved();
      }
    });
  },

  /**
   * Shows recipient suggestions if available, otherwise removes it from the DOM
   * and hides suggestions container.
   * @param {DocumentFragment} suggestions DocumentFragment node that contains
   * recipient suggestion to display.
   */
  toggleRecipientSuggestions: function(suggestions) {
    var contactList = this.recipientSuggestions.querySelector('.contact-list');

    this.recipientSuggestions.classList.toggle('hide', !suggestions);

    if (!suggestions) {
      contactList.textContent = '';
    } else {
      contactList.appendChild(suggestions);
      this.recipientSuggestions.scrollTop = 0;
    }
  },

  selectRecipientSuggestion: function(){
    var selected = document.querySelector('#messages-recipient-suggestions li.selected a');
    if (selected) {
      new Promise(function(resolve) {
        selected.click();
        resolve();
      }).then(() => {
        ThreadUI.stopContactNavigation();
        var recipient_focus = document.getElementById('messages-to-field');
        recipient_focus.lastChild.focus();
        ThreadUI.updateSKs(threadUiAddContact);
      });
    }
  },

  /**
   * If the bubble selection mode is disabled, all the non-editable element
   * should be set to user-select: none to prevent selection triggered
   * unexpectedly. Selection functionality should be enabled only by bubble
   * context menu. While in bubble selection mode, context menu is disabled
   * temporary for better use experience.
   * Since long press is used for context menu first, selection need to be
   * triggered by selection API manually. Focus/blur events are used for
   * simulating selection changed event, which is only been used in system.
   * When the node gets blur event, bubble selection mode should be dismissed.
   * @param {Object} node element that contains message bubble text content.
   */
  enableBubbleSelection: function(node) {
    var threadMessagesClass = this.threadMessages.classList;
    var disable = () => {
      this.container.addEventListener('contextmenu', this);
      node.removeEventListener('blur', disable);
      threadMessagesClass.add('editable-select-mode');
      // TODO: Remove this once the gecko could clear selection automatically
      // in bug 1101376.
      window.getSelection().removeAllRanges();
    };

    node.addEventListener('blur', disable);
    this.container.removeEventListener('contextmenu', this);
    threadMessagesClass.remove('editable-select-mode');
    node.focus();
    window.getSelection().selectAllChildren(node);
  },

  startContactNavigation: function() {
    this.removeAllFocusable();
    NavigationMap.reset('thread-messages');
    window.addEventListener('keydown', this.handleContactList);
    this.listener = true;
  },

  stopContactNavigation: function() {
    this.recoverAllFocusable();
    NavigationMap.reset('thread-messages');
    window.removeEventListener('keydown', this.handleContactList);
    this.listener = false;
  },

  removeAllFocusable: function() {
    this.focusableElement = document.querySelector('#thread-messages').querySelectorAll('.navigable');
    var elements = Array.prototype.slice.call(this.focusableElement);
    elements.forEach(function(element) {
      if (!element.classList.contains('recipient')) {
        element.classList.remove('navigable');
      }
    })
  },

  recoverAllFocusable: function() {
    if (this.focusableElement) {
      var elements = Array.prototype.slice.call(this.focusableElement);
      elements.forEach(function(element) {
        if (!element.classList.contains('recipient')) {
          element.classList.add('navigable');
        }
      });
      this.focusableElement = null;
    }
  },

  handleContactList: function(e) {
    var suggestionList = document.querySelectorAll('#messages-recipient-suggestions li');
    var selected = document.querySelector('#messages-recipient-suggestions li.selected');
    switch (e.key) {
      case 'ArrowUp':
        if (selected) {
          if (selected.previousSibling) {
            selected.classList.remove('selected');
            selected.previousSibling.classList.add('selected');
            document.querySelector('.selected').scrollIntoView(true);
          } else {
            selected.classList.remove('selected');
          }
        }
        e.preventDefault();
        break;
      case 'ArrowDown':
        if (selected) {
          if (selected.nextSibling) {
            selected.classList.remove('selected');
            selected.nextSibling.classList.add('selected');
            document.querySelector('.selected').scrollIntoView(false);
          } else {
            selected.classList.remove('selected');
            ThreadUI.stopContactNavigation();
            var index = ThreadUI.getItemIndex(NavigationMap.getCurrentControl().elements, 'messages-input');
            NavigationMap.setFocus(index);
          }
        } else {
          suggestionList[0].classList.add('selected');
        }
        e.preventDefault();
        break;
    }
  },

  subjectManagement: function() {
    if (Compose.isSubjectVisible) {
      Compose.hideSubject();
    } else {
      Compose.showSubject();
    }
    NavigationMap.navSetup('thread-messages', '.navigable');
    option.menuVisible = false;
    if (Compose.isSubjectVisible) {
      var index = this.getItemIndex(NavigationMap.getCurrentControl().elements, 'subject-composer-input');
      NavigationMap.setFocus(index);
    } else {
      var index = this.getItemIndex(NavigationMap.getCurrentControl().elements, 'messages-input');
      NavigationMap.setFocus(index);
      Toaster.showToast({
        messageL10nId: 'subject-removed',
        latency: this.CONVERTED_MESSAGE_DURATION
      });
    }
  },

  getItemIndex: function(elements, id) {
    var index, length = elements.length;
    for (var i = 0; i < length; i++) {
      if (elements[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  },

  _forwardMessage: function (messageId) {
    Navigation.toPanel('composer', {
      forward: {
        messageId: messageId
      }
    });
  },

  _getFocusedMessageId: function(forcusedItem) {
    if (forcusedItem.tagName != 'LI') {
      var item = this.getParents(forcusedItem, 'LI');
      return item.dataset.messageId;
    } else {
      return forcusedItem.dataset.messageId;
    }
  },

  longPressEvent: function() {
    if ((Navigation.isCurrentPanel('thread') || Navigation.isCurrentPanel('composer'))
        && (Compose.isFocused() || Compose.isSubjectFocused()
        || (!!ThreadUI.recipients && ThreadUI.recipients.isFocused()))) {
      if (Compose.isSubjectFocused()) {
        ThreadUI.subjectManagement();
      } else if (!!ThreadUI.recipients && ThreadUI.recipients.isFocused()) {
        ThreadUI.recipients.deleteAll();
        ThreadUI.recipients.focus();
      }
    }
  },

  updateSksOnRecipientChanged: function(lenght) {
    if (lenght === 0) {
      ThreadUI.updateSKs(threadUiAddContact);
    } else {
      ThreadUI.updateSKs(threadUIAddContactWithInput);
    }
  },

  getSksForSaveDraft: function(skParam) {
    var updateSkParam = {
      header: { l10nId:'options' },
      items: []
    };
    updateSkParam.items = Array.prototype.concat.apply([], skParam.items);
    updateSkParam.items.push(skSaveAsDraft);
    updateSkParam.items.push(skCancel);
    return updateSkParam;
  },

  onFocusChanged: function() {
    Utils.menuOptionVisible = false;
    ThreadUI.dynamicSK();
  },

  disableInputEnter: function(e) {
    if (e.key === 'Enter') {
      e.preventDefault();
    }
  },

  getFocusState: function(focusEl) {
    var focusState = ThreadUI.FOCUS_INVALID;
    if (focusEl.parentNode === ThreadUI.selector.recipient) {
      focusState = ThreadUI.FOCUS_ON_RECIPIENTS;
    } else if (focusEl === ThreadUI.selector.subject) {
      focusState = ThreadUI.FOCUS_ON_SUBJECT;
    } else if (focusEl === ThreadUI.selector.textField) {
      focusState = ThreadUI.FOCUS_ON_MESSAGE_INPUT;
    } else if (ThreadUI.getParents(focusEl, 'DIV') === ThreadUI.selector.textField
            && focusEl.tagName === 'IFRAME') {
      focusState = ThreadUI.FOCUS_ON_INPUT_IFRAME;
    } else if (ThreadUI.getParents(focusEl, 'UL').classList.contains('message-list')) {
      focusState = ThreadUI.FOCUS_ON_MESSAGE_THREAD;
    }
    return focusState;
  },

  dynamicComposerSK: function(focusState) {
    var skParam;
    switch (focusState) {
      case ThreadUI.FOCUS_ON_RECIPIENTS:
        if (!Compose.disableSendButton()) {
          skParam = threadUiAddContact_send;
          skParam = ThreadUI.selectSIMSend(skParam);
        } else {
          skParam = threadUiAddContact;
        }
        break;
      case ThreadUI.FOCUS_ON_SUBJECT:
        if (!Compose.disableSendButton()) {
          skParam = threadUiComposerOptionsWithSubRemove_Send_Subfocused;
          skParam = ThreadUI.selectSIMSend(skParam);
        } else {
          skParam = threadUiComposerOptionsWithSubRemove_subfocused;
        }
        if (ThreadUI.recipients.length || !Compose.isEmpty()) {
          skParam = ThreadUI.getSksForSaveDraft(skParam);
        }
        break;
      case ThreadUI.FOCUS_ON_MESSAGE_INPUT:
        if (!Compose.disableSendButton()) {
          if (Compose.isSubjectVisible) {
            skParam = threadUiComposerOptionsWithSubRemove_Send;
          } else {
            if (Settings.mmsEnable) {
              skParam = threadUiComposerOptionsWithSend;
            } else {
              skParam = threadUiComposerOptionsWithSend_noMMS;
            }
          }
          skParam = ThreadUI.selectSIMSend(skParam);
        } else if (Compose.isSubjectVisible) {
          skParam = threadUiComposerOptionsWithSubRemove;
        } else {
          if (Settings.mmsEnable) {
            skParam = threadUiComposerOptions;
          } else {
            skParam = threadUiComposerOptions_noMMS;
          }
        }
        if (ThreadUI.recipients.length || !Compose.isEmpty()) {
          skParam = ThreadUI.getSksForSaveDraft(skParam);
        }

        if (Compose.size > Settings.mmsSizeLimitation &&
            skParam.items[0].l10nId === 'send') {
          skParam.items.splice(0, 1);
        }
        break;
      case ThreadUI.FOCUS_ON_INPUT_IFRAME:
        if (!Compose.disableSendButton()) {
          skParam = threadUiComposerOptionsWithSend_Attach;
          skParam = ThreadUI.selectSIMSend(skParam);
        } else {
          skParam = threadUiComposerOptionsWithAttach;
        }

        if (Compose.size > Settings.mmsSizeLimitation &&
            skParam.items[0].l10nId === 'send') {
          skParam.items.splice(0, 1);
        }
        break;
      default:
        break;
    }
    ThreadUI.updateSKs(skParam);
  },

  dynamicThreadSK: function(focusState, focusEl) {
    var skParam;
    switch (focusState) {
      case ThreadUI.FOCUS_INVALID:
        return;
        break;
      case ThreadUI.FOCUS_ON_SUBJECT:
        if (Compose.isEmpty()) {
          skParam = threadUiNormalOptionsWithSubRemove_Subfocused;
        } else {
          skParam = ThreadUI
            .getSksForSaveDraft(threadUiNormalOptionsWithSubRemove_Send_Subfocused);
          skParam = ThreadUI.selectSIMSend(skParam);
        }
        break;
      case ThreadUI.FOCUS_ON_MESSAGE_INPUT:
        if (Compose.isEmpty()) {
          if (Compose.isSubjectVisible) {
            skParam = threadUiNormalOptionsWithSubRemove;
          } else {
            if (Settings.mmsEnable) {
              skParam = threadUiNormalOptions;
            } else {
              skParam = threadUiNormalOptions_noMMS;
            }
          }
        } else {
          if (Compose.isSubjectVisible) {
            skParam = threadUiNormalOptionsWithSubRemove_Send;
          } else {
            if (Settings.mmsEnable) {
              skParam = threadUiNormalOptionsWithSend;
            } else {
              skParam = threadUiNormalOptionsWithSend_noMMS;
            }
          }
          skParam = ThreadUI.selectSIMSend(skParam);
          skParam = ThreadUI.getSksForSaveDraft(skParam);

          if (Compose.size > Settings.mmsSizeLimitation &&
              skParam.items[0].l10nId === 'send') {
            skParam.items.splice(0, 1);
          }
         }
        break;
      case ThreadUI.FOCUS_ON_INPUT_IFRAME:
        skParam = threadUiNormalOptionsWithSend_Attach;
        skParam = ThreadUI.selectSIMSend(skParam);

        if (Compose.size > Settings.mmsSizeLimitation &&
            skParam.items[0].l10nId === 'send') {
          skParam.items.splice(0, 1);
        }
        break;
      case ThreadUI.FOCUS_ON_MESSAGE_THREAD:
        ThreadUI._storeFocused = focusEl;
        break;
      default:
        break;
    }
    ThreadUI.updateSKs(skParam);
  },

  dynamicSK: function(evt) {
    if (window.option && window.option.menuVisible) {
      return;
    }
    if (ThreadUI.confirmDialogShown) {
      console.log('confirm dialog is showing');
      return;
    }
    var currentFocus = document.querySelector('.focus');
    if (!currentFocus) {
      return;
    }
    if (document.activeElement.id !== currentFocus.id) {
      currentFocus.focus();
    }
    var focusState = ThreadUI.getFocusState(currentFocus);
    if (Navigation.isCurrentPanel('composer')) {
      ThreadUI.dynamicComposerSK(focusState);
    } else if (Navigation.isCurrentPanel('thread')) {
      ThreadUI.dynamicThreadSK(focusState, currentFocus);
    }
  },

  // Check wifi for wifi calling
  isWifiCallingAvaiable: function(serviceId) {
    var conn = navigator.mozMobileConnections && navigator.mozMobileConnections[serviceId];
    // If there is not useful SIM card, continue sending action to gecko.
    if (conn && conn.iccId === null) {
      return true;
    }
    // When conn.voice.relSignalStrength is 0 means lost signal or airplane mode on
    // We need consider the data because 4G-LTE not have voice.
    if ((!conn || (!conn.voice && !conn.data)) || (conn &&
        ((conn.voice && ((conn.signalStrength.level === 0) ||
          (!conn.voice.connected))) ||
         (conn.data && ((conn.signalStrength.level === 0) ||
          (conn.data.state !== 'registered' &&
           conn.data.state !== 'searching')))))) {
      // Check wifi information for wifi-call
      var wifiManager = navigator.mozWifiManager;
      if (conn && conn.imsHandler && conn.imsHandler.capability) {
        return true;
      } else if (wifiManager.enabled && wifiManager.connection.status === 'disconnected') {
        this.showMessageError('NeedConnectWifiError');
        return false;
      } else if (conn && conn.radioState === 'enabled' && conn.iccId !== null) {
        // Not plane mode, not no SIM card
        this.showMessageError('NoSignalError');
        return false;
      }
    }

    // Handle other situations by message sending process.
    return true;
  },

  isMediaStorageExceed: function () {
    var storages = navigator.getDeviceStorages('sdcard')[0];
    var freereq = storages.freeSpace();
    var limitsize = 10 * 1024 * 1024;
    freereq.onsuccess = function () {
      function confirmCallback() {
        var activity = new window.MozActivity({
          name: 'configure',
          data: {
            target: 'device',
            section: 'mediaStorage'
          }
        });
        ThreadUI.confirmDialogShown = false;
      }

      function cancelCallback() {
        ThreadUI.confirmDialogShown = false;
      }

      if ((freereq.result < limitsize) &&
          (Navigation.isCurrentPanel('thread-list') ||
           Navigation.isCurrentPanel('composer'))) {
        Utils.confirmAlert('media-storage-limit-alert-title',
                           'media-storage-limit-alert-body',
                           'media-storage-limit-alert-cancel',
                           cancelCallback, null, null,
                           'media-storage-limit-alert-setting',
                           confirmCallback, cancelCallback);
        ThreadUI.confirmDialogShown = true;
      }
    };
  },

  startSendMessage: function () {
    var serviceId = Settings.smsServiceId;
    if (serviceId === null) {
      serviceId = 0;
    }
    if (serviceId === -1) {
      if (Settings.hasSeveralSim()) {
        // If there is option menu, not create it again.
        if (!document.getElementById('option-menu')) {
          ThreadUI.simSelectOptions();
        }
      } else {
        // If there is single SIM, the serviceIds length is 0,
        // if there are dual SIMs, the serviceIds length is 2.
        if (Settings._serviceIds.length === 2) {
          // If all serviceIds are null, there is no SIM.
          if (Settings._serviceIds[0] === null &&
              Settings._serviceIds[1] === null) {
            this.showMessageError('NoSimCardError');
            return;
          }
        }

        var simServiceId = ThreadUI.selectSIMCard();
        ThreadUI.sendMessage({serviceId: simServiceId});
      }
     // Defect407-ting-wang@t2mobile.com-]There is no reaction to send SMS when the SIM card is not inserted
    } else if (serviceId === 0 || serviceId === 1) {
      ThreadUI.sendMessage({serviceId: serviceId});
    } else {
      console.log('Not handle serviceId = ' + serviceId);
    }
  },

  selectSIMCard: function () {
    for (var i = 0; i < Settings._serviceIds.length; i++) {
      if (Settings._serviceIds[i] !== null &&
          Settings._serviceIds[i].iccId !== null) {
        return i;
      }
    }
  },

  selectSIMSend: function (params) {
    if (params.items.indexOf(skSendSIM1) !== -1 ||
      params.items.indexOf(skSendSIM2) !== -1 ||
      params.items.indexOf(skSend) !== -1) {
      return params;
    }
    if (Settings.isDualSimDevice() && Settings.hasSeveralSim()) {
      switch (Settings.smsServiceId) {
        case 0:
          params.items.unshift(skSendSIM1);
          break;
        case 1:
          params.items.unshift(skSendSIM2);
          break;
        case -1:
          params.items.unshift(skSend);
          break;
        default:
          break;
      }
    } else {
      params.items.unshift(skSend);
    }

    return params;
  }
};

Object.defineProperty(ThreadUI, 'allInputs', {
  get: function() {
    return this.getAllInputs();
  }
});

Object.defineProperty(exports, 'ThreadUI', {
  get: function () {
    delete exports.ThreadUI;

    var allowedEvents = ['recipientschange'];
    return (exports.ThreadUI = EventDispatcher.mixin(ThreadUI, allowedEvents));
  },
  configurable: true,
  enumerable: true
});
}(this));
